from sqlalchemy import Column, Integer, String, Date, JSON, ForeignKey, Text, text
from flask import current_app as app

db = app.config.get('DB')

def insert(record):
    db.session.add(record)
    db.session.commit()

def delete(record):
    db.session.delete(record)
    db.session.commit()

def update(oldrecord, newrecord):
    oldrecord.update(newrecord)
    db.session.commit()

def select_by_label(etiqueta):
    sql = text(f'''
        SELECT *
        FROM documentos
        WHERE JSON_SEARCH(etiquetas, "one", "{etiqueta}") IS NOT NULL
    ''')
    result = db.engine.execute(sql)
    return result

class TipoDocumento(db.Model):
    __tablename__ = 'tipos_documentos'

    codigo = Column(Integer, primary_key=True)
    nombre = Column(String, nullable=False)
    descripcion = Column(String)

    def __init__(self, nombre=None, descripcion=None):
        self.nombre = nombre
        self.descripcion = descripcion

class Eje(db.Model):
    __tablename__ = 'ejes'

    codigo = Column(Integer, primary_key=True)
    nombre = Column(String, nullable=False)
    descripcion = Column(String)

    def __init__(self, nombre=None, descripcion=None):
        self.nombre = nombre
        self.descripcion = descripcion

class Condicion(db.Model):
    __tablename__ = 'condiciones'

    codigo = Column(Integer, primary_key=True)
    nombre = Column(String, nullable=False)
    descripcion = Column(String)
    eje = Column(Integer, ForeignKey('ejes.codigo'), nullable=False)

    def __init__(self, nombre=None, descripcion=None, eje=None):
        self.nombre = nombre
        self.descripcion = descripcion
        self.eje = eje

class Usuario(db.Model):
    __tablename__ = 'usuarios'

    identificacion = Column(String, primary_key=True)
    nombre = Column(String, nullable=False)
    correo = Column(String, nullable=False)
    clave = Column(String, nullable=False)
    estado = Column(String, nullable=False)

    def __init__(self, identificacion=None, nombre=None, correo=None,
        clave=None, estado=None):
        self.identificacion = identificacion
        self.nombre = nombre
        self.correo = correo
        self.clave = clave
        self.estado = estado

class Organizacion(db.Model):
    __tablename__ = 'organizaciones'

    nombre = Column(String, primary_key=True)
    correo = Column(String)
    pagina_web = Column(String)
    telefono = Column(String)
    sigla = Column(String)
    presentacion = Column(Text)
    condicion = Column(Integer, ForeignKey('condiciones.codigo'), nullable=False)

    def __init__(self, nombre=None, correo=None, pagina_web=None, telefono=None,
        sigla=None, presentacion=None, condicion=None):
        self.nombre = nombre
        self.correo = correo
        self.pagina_web = pagina_web
        self.telefono = telefono
        self.sigla = sigla
        self.presentacion = presentacion
        self.condicion = condicion

class Certificacion(db.Model):
    __tablename__ = 'certificaciones'

    codigo = Column(Integer, primary_key=True)
    nombre = Column(String, nullable=False)
    descripcion = Column(String)
    entidad = Column(String)
    referencia = Column(String)
    condicion = Column(Integer, ForeignKey('condiciones.codigo'), nullable=False)

    def __init__(self, nombre=None, descripcion=None, entidad=None,
        referencia=None, condicion=None):
        self.nombre = nombre
        self.descripcion = descripcion
        self.entidad = entidad
        self.referencia = referencia
        self.condicion = condicion

class Documento(db.Model):
    __tablename__ = 'documentos'

    codigo = Column(Integer, primary_key=True)
    titulo = Column(String, nullable=False)
    descripcion = Column(String)
    fecha = Column(Date, nullable=False)
    entidad_autor = Column(String)
    pais_region = Column(String)
    referencia = Column(String)
    categoria = Column(String)
    ambito = Column(String)
    etiquetas = Column(JSON)
    usuario = Column(String, ForeignKey('usuarios.identificacion'), nullable=False)
    eje = Column(Integer, ForeignKey('ejes.codigo'), nullable=False)
    tipo = Column(Integer, ForeignKey('tipos_documentos.codigo'), nullable=False)

    def __init__(self, titulo=None, descripcion=None, fecha=None, pais_region=None,
        entidad_autor=None, referencia=None, categoria=None, ambito=None, 
        etiquetas=None, usuario=None, eje=None, tipo=None):
        self.titulo = titulo
        self.descripcion = descripcion
        self.fecha = fecha
        self.entidad_autor = entidad_autor
        self.pais_region = pais_region
        self.referencia = referencia
        self.categoria = categoria
        self.ambito = ambito
        self.etiquetas = etiquetas
        self.usuario = usuario
        self.eje = eje
        self.tipo = tipo
