from flask import current_app as app
from api.db.models import TipoDocumento, Eje, Condicion, Usuario, Organizacion, Certificacion, Documento
from marshmallow import Schema, fields, validate

ma = app.config.get('MA_SERIALIZER')

class TipoDocumentoSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TipoDocumento

tipo_documento_schema = TipoDocumentoSchema()
tipos_documentos_schema = TipoDocumentoSchema(many=True)

class EjeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Eje

eje_schema = EjeSchema()
ejes_schema = EjeSchema(many=True)

class CondicionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Condicion
        include_fk = True

condicion_schema = CondicionSchema()
condiciones_schema = CondicionSchema(many=True)

class UsuarioSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Usuario

usuario_schema = UsuarioSchema()
usuarios_schema = UsuarioSchema(many=True)

class OrganizacionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Organizacion
        include_fk = True

organizacion_schema = OrganizacionSchema()
organizaciones_schema = OrganizacionSchema(many=True)

class CertificacionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Certificacion
        include_fk = True

certificacion_schema = CertificacionSchema()
certificaciones_schema = CertificacionSchema(many=True)

class DocumentoSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Documento
        include_fk = True

documento_schema = DocumentoSchema()
documentos_schema = DocumentoSchema(many=True)

class LoginSchema(Schema):
    correo = fields.String()
    clave = fields.String(validate=[validate.Regexp(r'\b[A-Fa-f0-9]{64}\b')])

login_schema = LoginSchema()
