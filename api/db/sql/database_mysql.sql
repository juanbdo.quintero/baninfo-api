-- **********************************
-- *********** Database *************
-- **********************************

CREATE DATABASE IF NOT EXISTS baninfo;
USE baninfo;

-- **********************************
-- ************ Tables **************
-- **********************************

-- table documentos
CREATE TABLE  documentos (
	usuario VARCHAR (30) NOT NULL 	,
	eje INTEGER (6) NOT NULL 	,
	tipo INTEGER (6) NOT NULL 	,
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	titulo VARCHAR (500) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	fecha DATE NOT NULL 	,
	entidad_autor VARCHAR (500) NULL 	,
	pais_region VARCHAR (500) NULL 	,
	referencia VARCHAR (500) NULL 	,
	categoria VARCHAR (15) NULL 	,
	ambito VARCHAR (15) NULL 	,
	etiquetas JSON NULL 	,
	CONSTRAINT  PK_Documento 
	PRIMARY KEY( codigo )
);

-- table tiposdocumentos
CREATE TABLE  tipos_documentos (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	CONSTRAINT  PK_TipoDocumento 
	PRIMARY KEY( codigo )
);

-- table usuarios
CREATE TABLE  usuarios (
	identificacion VARCHAR (30) NOT NULL 	,
	nombre VARCHAR (200) NOT NULL 	,
	correo VARCHAR (200) NOT NULL 	,
	clave VARCHAR (100) NOT NULL 	,
	estado VARCHAR (50) NOT NULL 	,
	CONSTRAINT  PK_Usuario 
	PRIMARY KEY( identificacion )
);

-- table ejes
CREATE TABLE  ejes (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	CONSTRAINT  PK_Eje 
	PRIMARY KEY( codigo )
);

-- table criterios
CREATE TABLE  condiciones (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	eje INTEGER (6) NOT NULL 	,
	CONSTRAINT  PK_Condicion 
	PRIMARY KEY( codigo )
);

-- table certificaciones
CREATE TABLE  certificaciones (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	entidad VARCHAR (500) NULL 	,
	referencia VARCHAR (500) NULL 	,
	condicion INTEGER (6) NOT NULL 	,
	CONSTRAINT  PK_Certificacion
	PRIMARY KEY( codigo )
);

-- table organizaciones
CREATE TABLE  organizaciones (
	nombre VARCHAR (200) NOT NULL 	,
	correo VARCHAR (200) NULL 	,
	pagina_web VARCHAR (500) NULL 	,
	telefono VARCHAR (200) NULL 	,
    sigla VARCHAR (30) NULL 	,
    presentacion TEXT NULL 	,
	condicion INTEGER (6) NOT NULL 	,
	CONSTRAINT  PK_Organizacion 
	PRIMARY KEY( nombre )
);

-- ****************************************
-- ************ Foreign Keys **************
-- ****************************************

-- For documentos(FK_Documento_Usuario) 
ALTER TABLE documentos ADD(
	CONSTRAINT FK_Documento_Usuario
	FOREIGN KEY ( usuario )
	REFERENCES  usuarios ( identificacion )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- For documentos(FK_Documento_Eje) 
ALTER TABLE documentos ADD(
	CONSTRAINT FK_Documento_Eje
	FOREIGN KEY ( eje )
	REFERENCES  ejes ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- For documentos(FK_Documento_TipoDocumento) 
ALTER TABLE documentos ADD(
	CONSTRAINT FK_Documento_TipoDocumento
	FOREIGN KEY ( tipo )
	REFERENCES  tipos_documentos ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- For condiciones(FK_Condicion_Eje) 
ALTER TABLE condiciones ADD(
	CONSTRAINT FK_Condicion_Eje
	FOREIGN KEY ( eje )
	REFERENCES  ejes ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- For certificaciones(FK_Certificacion_Condicion) 
ALTER TABLE certificaciones ADD(
	CONSTRAINT FK_Certificacion_Condicion
	FOREIGN KEY ( condicion )
	REFERENCES  condiciones ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- For organizaciones(FK_Organizacion_Condicion) 
ALTER TABLE organizaciones ADD(
	CONSTRAINT FK_Organizacion_Condicion
	FOREIGN KEY ( condicion )
	REFERENCES  condiciones ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
