-- **********************************
-- *********** Database *************
-- **********************************
create database if not exists baninfo;
use baninfo;
-- **********************************
-- ************ Tables **************
-- **********************************

-- table documentos
CREATE TABLE  documentos (
	usuario VARCHAR (30) NOT NULL 	,
	eje INTEGER (6) NOT NULL 	,
	tipo INTEGER (6) NOT NULL 	,
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	titulo VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	fecha DATE NOT NULL 	,
	entidad_autor VARCHAR (500) NULL 	,
	pais_region VARCHAR (500) NULL 	,
	referencia VARCHAR (500) NULL 	,
	categoria VARCHAR (15) NULL 	,
	ambito VARCHAR (15) NULL 	,
	etiquetas JSON NULL 	,
	CONSTRAINT  pk_Documento 
	PRIMARY KEY( codigo )
);

-- table tiposdocumentos
CREATE TABLE  tipos_documentos (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	CONSTRAINT  pk_TipoDocumento 
	PRIMARY KEY( codigo )
);

-- table usuarios
CREATE TABLE  usuarios (
	identificacion VARCHAR (30) NOT NULL 	,
	nombre VARCHAR (200) NOT NULL 	,
	correo VARCHAR (200) NOT NULL 	,
	clave VARCHAR (100) NOT NULL 	,
	estado VARCHAR (50) NOT NULL 	,
	CONSTRAINT  pk_Usuario 
	PRIMARY KEY( identificacion )
);

-- table ejes
CREATE TABLE  ejes (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	CONSTRAINT  pk_Eje 
	PRIMARY KEY( codigo )
);

-- table criterios
CREATE TABLE  criterios (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	eje INTEGER (6) NOT NULL 	,
	CONSTRAINT  pk_Criterio 
	PRIMARY KEY( codigo )
);

-- table certificaciones
CREATE TABLE  certificaciones (
	codigo INTEGER (6) NOT NULL AUTO_INCREMENT	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	entidad VARCHAR (500) NULL 	,
	referencia VARCHAR (500) NULL 	,
	criterio INTEGER (6) NOT NULL 	,
	CONSTRAINT  pk_Certificacion
	PRIMARY KEY( codigo )
);

-- table organizaciones
CREATE TABLE  organizaciones (
	nombre VARCHAR (200) NOT NULL 	,
	correo VARCHAR (200) NULL 	,
	pagina_web VARCHAR (500) NULL 	,
	telefono VARCHAR (200) NULL 	,
	CONSTRAINT  pk_Organizacion 
	PRIMARY KEY( nombre )
);

-- table proveedores
CREATE TABLE  proveedores (
	organizacion VARCHAR (200) NOT NULL 	,
	criterio INTEGER (6) NOT NULL 	,
	CONSTRAINT  pk_Proveedor 
	PRIMARY KEY( criterio, organizacion )
);

-- ****************************************
-- ************ Foreign Keys **************
-- ****************************************

-- For documentos(fk_Documento_Usuario) 
ALTER TABLE documentos ADD(
	CONSTRAINT fk_Documento_Usuario
	FOREIGN KEY ( usuario )
	REFERENCES  usuarios ( identificacion )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For documentos(fk_Documento_Eje) 
ALTER TABLE documentos ADD(
	CONSTRAINT fk_Documento_Eje
	FOREIGN KEY ( eje )
	REFERENCES  ejes ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For documentos(fk_Documento_TipoDocumento) 
ALTER TABLE documentos ADD(
	CONSTRAINT fk_Documento_TipoDocumento
	FOREIGN KEY ( tipo )
	REFERENCES  tipos_documentos ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For criterios(fk_Criterio_Eje) 
ALTER TABLE criterios ADD(
	CONSTRAINT fk_Criterio_Eje
	FOREIGN KEY ( eje )
	REFERENCES  ejes ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For certificaciones(fk_Certificacion_Criterio) 
ALTER TABLE certificaciones ADD(
	CONSTRAINT fk_Certificacion_Criterio
	FOREIGN KEY ( criterio )
	REFERENCES  criterios ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For proveedores(fk_Proveedor_Criterio) 
ALTER TABLE proveedores ADD(
	CONSTRAINT fk_Proveedor_Criterio
	FOREIGN KEY ( criterio )
	REFERENCES  criterios ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For proveedores(fk_Proveedor_Organizacion) 
ALTER TABLE proveedores ADD(
	CONSTRAINT fk_Proveedor_Organizacion
	FOREIGN KEY ( organizacion )
	REFERENCES  organizaciones ( nombre )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
