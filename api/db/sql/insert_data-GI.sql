-- ***********************************************
-- Table: organizaciones (Grupos de Investigación)
-- ***********************************************
INSERT INTO organizaciones (condicion,nombre,presentacion) VALUES 
(12,'Policía y sociedad', 'Código del Grupo: COL0064807 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Dirección Nacional De Escuelas - Policía Nacional De Colombia
Municipio: La Estrella'),
(12,'Administración y organizaciones', 'Código del Grupo: COL0013011 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Administraciòn, gobierno pùblico y ambiente', 'Código del Grupo: COL0098716 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Aerospace science and technology research (astra)', 'Código del Grupo: COL0164301 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Aglaia', 'Código del Grupo: COL0081361 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Corporación Universitaria Americana
Municipio: Medellín'),
(12,'Agrobiotecnología', 'Código del Grupo: COL0122947 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Agroxue', 'Código del Grupo: COL0113259 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Álgebra U de A', 'Código del Grupo: COL0086896 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Alimentación y nutrición humana', 'Código del Grupo: COL0000407 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'ALSEC Alimentos Secos S.A.S', 'Código del Grupo: COL0187342 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: ALSEC Alimentos Secos S.A.S
Municipio: La Estrella'),
(12,'Americana emprendedora', 'Código del Grupo: COL0199094 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporación Universitaria Americana
Municipio: Medellín'),
(12,'Amyc investigaciones', 'Código del Grupo: COL0192369 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Administrative Management Consultants S.A.S.
Municipio: Medellín'),
(12,'Análisis de residuos', 'Código del Grupo: COL0000461 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Análisis multivariado', 'Código del Grupo: COL0000532 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Análisis numérico y financiero: matemáticas aplicadas para la industria.', 'Código del Grupo: COL0106371 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Math Decision S.A.S. | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Aplicaciones en fotoquímica', 'Código del Grupo: COL0137108 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Aplicaciones estadísticas y salud pública', 'Código del Grupo: COL0141693 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Aprendizaje e innovación educativa', 'Código del Grupo: COL0143123 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Área de ciencias del mar Universidad EAFIT', 'Código del Grupo: COL0000185 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Arkadius', 'Código del Grupo: COL0035814 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Arquitectura, urbanismo y paisaje', 'Código del Grupo: COL0023457 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Artes escénicas y del espectáculo', 'Código del Grupo: COL0069339 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Artes y humanidades', 'Código del Grupo: COL0077269 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Artes y modelos de pensamiento', 'Código del Grupo: COL0054936 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Atención primaria en salud', 'Código del Grupo: COL0136316 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Auditorio constitucional', 'Código del Grupo: COL0033776 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Institución Universitaria de Envigado
Municipio: Envigado'),
(12,'Automática, electrónica y ciencias computacionales', 'Código del Grupo: COL0053581 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Bacterias & cáncer', 'Código del Grupo: COL0070457 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Bacteriología agrícola y ambiental', 'Código del Grupo: COL0177819 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biblia y teología', 'Código del Grupo: COL0105211 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Fundacion Universitaria Seminario Biblico De Colombia
Municipio: Medellín'),
(12,'Big data y data analytics', 'Código del Grupo: COL0043351 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Bioali biotecnología de alimentos', 'Código del Grupo: COL0038379 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biociencias', 'Código del Grupo: COL0050098 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Institución Universitaria Colegio Mayor de Antioquia
Municipio: Medellín'),
(12,'Biocontrol y microbiología ambiental -bioma-', 'Código del Grupo: COL0039509 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biodiversidad', 'Código del Grupo: COL0041409 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Corporación para Investigaciones Biológicas
Municipio: Medellín'),
(12,'Biodiversidad y genética molecular "biogem"', 'Código del Grupo: COL0053919 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Animales y Lechería
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biodiversidad, evolución y conservación (BEC)', 'Código del Grupo: COL0172339 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Bio-diversidades, género, cultura y política ambiental; grupo de investigación corporación ecológica cultural penca de sábila', 'Código del Grupo: COL0138188 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Geografía social y económica
Institución aval: CORPORACION ECOLOGICA Y CULTURAL PENCA DE SABILA
Municipio: Medellín'),
(12,'Biofibras y derivados vegetales', 'Código del Grupo: COL0150879 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Biotecnología Industrial
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Bioforense', 'Código del Grupo: COL0036114 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Otras Ciencias Médicas
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Biogénesis', 'Código del Grupo: COL0066561 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Universidad Nacional de Colombia - Sede Medellín | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Bioingeniería de cabeza y cuello', 'Código del Grupo: COL0197339 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Biología celular y molecular cib, U. de A. U. Del rosario', 'Código del Grupo: COL0000962 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Corporación para Investigaciones Biológicas | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biología CES', 'Código del Grupo: COL0061224 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Biología de sistemas', 'Código del Grupo: COL0075598 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Biología estructural y proteómica', 'Código del Grupo: COL0004479 | Clasificación:  C
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina clínica
Institución aval: Parque Tecnológico De Antioquia S.A. | Universidad Pontificia Bolivariana - Sede Medellín | Universidad Del Atlántico
Municipio: Medellín'),
(12,'Biología funcional', 'Código del Grupo: COL0067738 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biología y clínica', 'Código del Grupo: COL0102748 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biología y control de enfermedades infecciosas', 'Código del Grupo: COL0007865 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biomatic (biomecánica, materiales, tics, diseño y calidad para el sector cuero, plástico y caucho y sus cadenas productivas', 'Código del Grupo: COL0129409 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Itagüí'),
(12,'Biomicro', 'Código del Grupo: COL0111863 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Biotecnología Ambiental
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biomoléculas', 'Código del Grupo: COL0072498 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: UNIDAD DE INVESTIGACION GENETICA MOLECULAR, UNIGEM
Municipio: Medellín'),
(12,'Biopolimer', 'Código del Grupo: COL0065152 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Bioprocesos', 'Código del Grupo: COL0023715 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Bioprocesos y flujos reactivos', 'Código del Grupo: COL0072237 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biotecnología', 'Código del Grupo: COL0001084 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biotecnología animal', 'Código del Grupo: COL0001093 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biotecnología básica y aplicada', 'Código del Grupo: COL0185016 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Biotecnología en Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Biotecnología industrial', 'Código del Grupo: COL0001109 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biotecnología microbiana', 'Código del Grupo: COL0001119 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biotecnología ruminal y silvopastoreo "biorum"', 'Código del Grupo: COL0051818 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biotecnología vegetal', 'Código del Grupo: COL0001075 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Biotecnología vegetal Unalmed - cib', 'Código del Grupo: COL0000828 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Corporación para Investigaciones Biológicas | Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Bosques y cambio climático', 'Código del Grupo: COL0070134 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Business intelligence', 'Código del Grupo: COL0098331 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporación Universitaria Americana
Municipio: Medellín'),
(12,'Calidad de la educación y proyecto educativo institucional', 'Código del Grupo: COL0001825 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Calidad metrología y producción', 'Código del Grupo: COL0032643 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Calorimetría y termodinámica de procesos irreversibles', 'Código del Grupo: COL0022736 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Casos y estudios organizacionales - ceo', 'Código del Grupo: COL0013049 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Catálisis ambiental', 'Código del Grupo: COL0001941 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Catalizadores y adsorbentes', 'Código del Grupo: COL0061073 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Cccomposites (cements, ceramics and composites)', 'Código del Grupo: COL0099698 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Centauro', 'Código del Grupo: COL0001262 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Centro de investigaciones dermatológicas', 'Código del Grupo: COL0130733 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: I.P.S. Universitaria | Universidad de Antioquia | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Centro de desarrollo tecnológico aeroespacial para la defensa cetad', 'Código del Grupo: COL0159366 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: ESCUELA MILITAR DE AVIACIÓN MARCO FIDEL SUAREZ EMAVI | ESCUELA DE SUBOFICIALES DE LA  FAC ESUFA | FUERZA AÉREA COLOMBIANA | INTER-TELCO S.A.S | Centro de Desarrollo Aeroespacial Para la Defensa CETAD-CACOM-5
Municipio: Medellín'),
(12,'Centro de estudios en economía sistémica - Ecsim', 'Código del Grupo: COL0042416 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: FUNDACION ECSIM CENTRO DE ESTUDIOS EN ECONOMIA SISTEMICA
Municipio: Medellín'),
(12,'Centro de estudios en gobierno digital y movilidad', 'Código del Grupo: COL0104269 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: QUIPUX
Municipio: Medellín'),
(12,'Centro de estudios y de investigación en biotecnología -Cibiot-', 'Código del Grupo: COL0002152 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Centro de investigación de desarrollo y calidad - Cidca', 'Código del Grupo: COL0128154 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Compañia Nacional de Chocolates S.A.S
Municipio: Rionegro'),
(12,'Centro de investigación e innovación en ingeniería - i3', 'Código del Grupo: COL0106836 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Corporación Industrial Minuto de Dios
Municipio: Medellín'),
(12,'Centro de investigación en comportamiento organizacional', 'Código del Grupo: COL0094332 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Psicología
Institución aval: Cincel
Municipio: Medellín'),
(12,'Centro de investigación y consultoría organizacional-cinco-', 'Código del Grupo: COL0061162 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Centro de investigación y desarrollo andercol', 'Código del Grupo: COL0033023 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: ANHIDRIDOS Y DERIVADOS DE COLOMBIA
Municipio: Medellín'),
(12,'Centro de investigación y desarrollo cárnico', 'Código del Grupo: COL0032026 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Industria de Alimentos Zenú S.A.S.
Municipio: Medellín'),
(12,'Centro de investigación y desarrollo Colcafe', 'Código del Grupo: COL0102999 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Colcafe S.A.S.
Municipio: Medellín'),
(12,'Centro de investigación, innovación y desarrollo de materiales - Cidemat - anteriormente: grupo de corrosión y protección', 'Código del Grupo: COL0007927 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Centro de investigaciones básicas y aplicadas en veterinaria- Cibav', 'Código del Grupo: COL0153246 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Centro de investigaciones científicas del deporte antioqueño- Cinda', 'Código del Grupo: COL0154225 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Otras Ciencias Médicas
Institución aval: Indeportes Antioquia
Municipio: Medellín'),
(12,'Centro de investigaciones del banano - Cenibanano', 'Código del Grupo: COL0000318 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Asociacion de Bananeros de Colombia
Municipio: Carepa'),
(12,'Centro de investigaciones del clima de Colombia - Cíclico', 'Código del Grupo: COL0074419 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias de la tierra y medioambientales
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Centro de proyectos e investigaciones sísmicas', 'Código del Grupo: COL0002339 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Centro nacional de geoestadística (Cng)', 'Código del Grupo: COL0083437 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ceo', 'Código del Grupo: COL0086288 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Ciba centro de investigaciones biológicas y ambientales', 'Código del Grupo: COL0091985 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias biológicas
Institución aval: Sociedad de Mejoras Publicas de Medellin
Municipio: Medellín'),
(12,'Cibereducación fundación universitaria católica del norte', 'Código del Grupo: COL0027975 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Fundación Universitaria Católica del Norte
Municipio: Santa Rosa de Osos'),
(12,'Ciencia de glicoconjugados y compuestos funcionales', 'Código del Grupo: COL0191719 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Otras Ciencias Naturales
Institución aval: Fundación Glucid
Municipio: Medellín'),
(12,'Ciencia de los materiales', 'Código del Grupo: COL0002401 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ciencia de materiales avanzados', 'Código del Grupo: COL0046522 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ciencia y tecnología de materiales', 'Código del Grupo: COL0002439 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ciencia y tecnología del gas y uso racional de la energía', 'Código del Grupo: COL0002466 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ciencia, investigación y pedagogía', 'Código del Grupo: COL0112913 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Escuela Normal Superior del Bajo Cauca
Municipio: Caucasia'),
(12,'Ciencia, tecnología y sociedad más innovación (cts+i)', 'Código del Grupo: COL0032358 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Ciencia, tecnología, innovación y emprendimiento (citie)', 'Código del Grupo: COL0129329 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Ciencias administrativas', 'Código del Grupo: COL0085271 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Ciencias aplicadas a la actividad física y el deporte gricafde', 'Código del Grupo: COL0002537 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ciencias básicas', 'Código del Grupo: COL0002509 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Ciencias básicas aplicadas Tecnológico Antioquia-Cbata-', 'Código del Grupo: COL0147893 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Ciencias biológicas y bioprocesos (Cibiop)', 'Código del Grupo: COL0143589 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Ciencias de la decisión', 'Código del Grupo: COL0062393 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ciencias de la tierra y el espacio', 'Código del Grupo: COL0139185 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Ciencias empresariales escolme - Cee', 'Código del Grupo: COL0165265 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Institucion Universitaria Escolme
Municipio: Medellín'),
(12,'Clínica psicológica', 'Código del Grupo: COL0196655 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Clío historia empresarial', 'Código del Grupo: COL0171057 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Código', 'Código del Grupo: COL0116009 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Institución Universitaria Salazar y Herrera
Municipio: Medellín'),
(12,'Coinde', 'Código del Grupo: COL0064315 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Colombia: tradiciones de la palabra', 'Código del Grupo: COL0032759 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Comaefi (comunidad de aprendizaje currículo y didáctica)', 'Código del Grupo: COL0022342 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Communicare', 'Código del Grupo: COL0192816 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Medellín'),
(12,'Communis', 'Código del Grupo: COL0181287 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Comportamiento humano organizacional -comphor-', 'Código del Grupo: COL0002635 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Comprender didáctica de las ciencias sociales y formación ciudadana', 'Código del Grupo: COL0060853 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Computación científica', 'Código del Grupo: COL0060379 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Comunicación digital y discurso académico (cd&da)', 'Código del Grupo: COL0056869 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Fundación Universitaria Católica del Norte
Municipio: Medellín'),
(12,'Comunicación y estudios culturales', 'Código del Grupo: COL0004596 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Comunicación, educación y ciudadanías.', 'Código del Grupo: COL0087875 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Comunicación, organización y política', 'Código del Grupo: COL0025513 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Comunicación, periodismo y sociedad', 'Código del Grupo: COL0002671 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Conflicto y paz', 'Código del Grupo: COL0140711 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Conocimiento, filosofía, ciencia, historia y sociedad', 'Código del Grupo: COL0064487 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Conservación, uso y biodiversidad', 'Código del Grupo: COL0055011 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Constitucionalismo crítico y género', 'Código del Grupo: COL0183156 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Contas - contabilidad ambiente y sociedad', 'Código del Grupo: COL0042489 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Contracampo: grupo de investigación audiovisual', 'Código del Grupo: COL0151009 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Control óptimo de sistemas híbridos', 'Código del Grupo: COL0166529 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Computación y ciencias de la información
Institución aval: Universidad de Medellín - Udem
Municipio: Medellín'),
(12,'Conversaciones entre pedagogía y psicoanálisis', 'Código del Grupo: COL0067308 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Cosmograv', 'Código del Grupo: COL0147829 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Crs-tid center for research and surveillance of tropical and infectious diseases', 'Código del Grupo: COL0183076 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad Nacional de Colombia - Sede Medellín | CORPORACIÓN PARA LA INVESTIGACIÓN EN SALUD TROPICAL
Municipio: Medellín'),
(12,'Cuidado de la vida gicuvi', 'Código del Grupo: COL0173436 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Cuidado enfermería CES', 'Código del Grupo: COL0180646 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Biotecnología en Salud
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Cultura y gestión organizacional', 'Código del Grupo: COL0018869 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Cultura, política y desarrollo social', 'Código del Grupo: COL0003015 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Cultura, violencia y territorio', 'Código del Grupo: COL0003024 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Sociología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Datus: grupo interdisciplinario de investigación, evaluación y análisis de política pública', 'Código del Grupo: COL0162174 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Delta', 'Código del Grupo: COL0057043 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Democracia, derecho y justicia(s)', 'Código del Grupo: COL0049729 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Demografía y salud', 'Código del Grupo: COL0003249 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Derecho administrativo', 'Código del Grupo: COL0161892 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Derecho y poder', 'Código del Grupo: COL0003113 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Derecho y política', 'Código del Grupo: COL0123569 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Derecho
Institución aval: grupo de investigacion y editorial kavilando
Municipio: Medellín'),
(12,'Derecho y sociedad', 'Código del Grupo: COL0003122 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Derecho, sociedad  y globalización', 'Código del Grupo: COL0019739 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Santo Tomás de Aquino - Sede Bogotá - Usta | Universidad Santo Tomás - Medellín
Municipio: Medellín'),
(12,'Derecho, cultura y ciudad', 'Código del Grupo: COL0035716 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Bello'),
(12,'Derechos fundamentales y teoría política', 'Código del Grupo: COL0100583 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Derechos humanos y derecho penal Unisabaneta', 'Código del Grupo: COL0162423 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Derecho
Institución aval: Corporación Universitaria de Sabaneta
Municipio: Sabaneta'),
(12,'Dermatología', 'Código del Grupo: COL0019499 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'dermatología CES', 'Código del Grupo: COL0003338 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Determinantes sociales y económicos de la situación de salud y nutrición', 'Código del Grupo: COL0065608 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Diagnóstico y control de la contaminación', 'Código del Grupo: COL0040402 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Didáctica y modelamiento en ciencias exactas y aplicadas (Da Vinci)', 'Código del Grupo: COL0028309 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Didáctica y nuevas tecnologías', 'Código del Grupo: COL0003561 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Didarte. Didáctica de las artes', 'Código del Grupo: COL0078615 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Dike - estudios en doctrina social y ética social', 'Código del Grupo: COL0039546 | Clasificación:  D
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad Pontificia Bolivariana - Sede Medellín
Municipio: Medellín'),
(12,'Dinámicas urbano-regionales', 'Código del Grupo: COL0003589 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Geografía Social y Económica
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Dirección de empresas', 'Código del Grupo: COL0100206 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institución Universitaria Esumer
Municipio: Medellín'),
(12,'Diseño y formulación de medicamentos, cosméticos y afines', 'Código del Grupo: COL0003623 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Diseño y optimización aplicada (Doa)', 'Código del Grupo: COL0169881 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Diverser (pedagogía y diversidad cultural)', 'Código del Grupo: COL0009108 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'E - management', 'Código del Grupo: COL0102176 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Fundación Universitaria Católica del Norte
Municipio: Medellín'),
(12,'Ecco - emoción, cognición y comportamiento', 'Código del Grupo: COL0085478 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Eco', 'Código del Grupo: COL0041034 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Politécnico Arzobispo Salazar Y Herrera
Municipio: Medellín'),
(12,'Ecoarte', 'Código del Grupo: COL0167199 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Academia Superior De Artes
Municipio: Medellín'),
(12,'Ecofisiología agraria', 'Código del Grupo: COL0037238 | Clasificación:  B
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ecología microbiana y bioprospección', 'Código del Grupo: COL0010682 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ecología y conservación de fauna silvestre', 'Código del Grupo: COL0068199 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ecología y evolución de vertebrados', 'Código del Grupo: COL0147267 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Economía de la salud', 'Código del Grupo: COL0004059 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Economía, cultura y políticas', 'Código del Grupo: COL0054069 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Económicas y contables Udecolombia', 'Código del Grupo: COL0181302 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: CORPORACIÓN UNIVERSITARIA U DE COLOMBIA
Municipio: Medellín'),
(12,'Ecosol', 'Código del Grupo: COL0003749 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Edi-educación y diversidad internacional', 'Código del Grupo: COL0157666 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Educación en ambientes virtuales', 'Código del Grupo: COL0004148 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Educación matemática e historia (UdeA - EAFIT)- Edumath', 'Código del Grupo: COL0004139 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad EAFIT | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Educación y derechos humanos', 'Código del Grupo: COL0158832 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Educación y desarrollo', 'Código del Grupo: COL0067845 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Educación y subjetividad', 'Código del Grupo: COL0082681 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Educación, infancia y lenguas extranjeras', 'Código del Grupo: COL0044789 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Educación, lenguaje y cognición', 'Código del Grupo: COL0004175 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Eeco -ecología evolutiva y conservación', 'Código del Grupo: COL0147239 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias biológicas
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Eia física teórica y aplicada', 'Código del Grupo: COL0044189 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Escuela de Ingeniería de Antioqua
Municipio: Envigado'),
(12,'El método analítico y sus aplicaciones en las ciencias sociales y humanas', 'Código del Grupo: COL0044878 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad EAFIT | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Electromagnetismo aplicado', 'Código del Grupo: COL0004273 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Elice-ecología lótica: islas, costas y estuarios', 'Código del Grupo: COL0101759 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Emac - enseñanza de matemáticas y computación', 'Código del Grupo: COL0180557 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Emancipaciones y contra-emancipaciones', 'Código del Grupo: COL0130143 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias políticas
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Emergencias y desastres', 'Código del Grupo: COL0023804 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Energeia', 'Código del Grupo: COL0199029 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Escuela de Ingeniería de Antioqua
Municipio: Envigado'),
(12,'Enfermedades del corazón ces', 'Código del Grupo: COL0041339 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES | Clínica CES
Municipio: Medellín'),
(12,'Enfocar', 'Código del Grupo: COL0120085 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Engineeri@', 'Código del Grupo: COL0198266 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Corporación Universitaria Americana
Municipio: Medellín'),
(12,'Enseñanza y aprendizaje de lenguas extranjeras -EALE-', 'Código del Grupo: COL0004344 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Entre otros', 'Código del Grupo: COL0066427 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Colegiatura Colombiana Institución Universitaria
Municipio: Medellín'),
(12,'Epidemiología', 'Código del Grupo: COL0004362 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Epidemiología hospitalaria', 'Código del Grupo: COL0057983 | Clasificación:  C
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina clínica
Institución aval: Hospital Universitario San Vicente Fundación
Municipio: Medellín'),
(12,'Epidemiología y bioestadística', 'Código del Grupo: COL0059709 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Epimeleia', 'Código del Grupo: COL0021514 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Escuela de prospectiva y desarrollo empresarial', 'Código del Grupo: COL0018072 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institución Universitaria Esumer
Municipio: Medellín'),
(12,'Escuela del hábitat-cehap', 'Código del Grupo: COL0003839 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Nacional de Colombia - Sede Medellín | Arkebios S.A.S.
Municipio: Medellín'),
(12,'Espiral', 'Código del Grupo: COL0055183 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingeniería civil
Institución aval: Escuela De Ingeniería De Antioquia - E.I.A.
Municipio: Envigado'),
(12,'Estabilidad estructural', 'Código del Grupo: COL0003866 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Estadistica industrial', 'Código del Grupo: COL0025961 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Estado de derecho y justicias', 'Código del Grupo: COL0128359 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estomatología biomédica', 'Código del Grupo: COL0118846 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estructuras y construcción', 'Código del Grupo: COL0053518 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingeniería civil
Institución aval: Escuela De Ingeniería De Antioquia - E.I.A.
Municipio: Envigado'),
(12,'Estudios biosociales del cuerpo -ebsc-', 'Código del Grupo: COL0108367 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estudios clínicos y sociales en psicología', 'Código del Grupo: COL0004587 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Estudios culturales sobre las ciencias y su enseñanza -ecce', 'Código del Grupo: COL0002644 | Clasificación:  A
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Estudios de derecho y sociedad', 'Código del Grupo: COL0111291 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Corporación Universitaria de Sabaneta
Municipio: Sabaneta'),
(12,'Estudios de fenómenos psicosociales', 'Código del Grupo: COL0043398 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Estudios del mundo del trabajo', 'Código del Grupo: COL0004694 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Escuela Nacional Sindical - E.N.S.
Municipio: Medellín'),
(12,'Estudios empresariales', 'Código del Grupo: COL0004611 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Estudios en cultura audiovisual', 'Código del Grupo: COL0131679 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Estudios en educación corporal', 'Código del Grupo: COL0043093 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estudios en filosofía, hermenéutica y narrativas', 'Código del Grupo: COL0046765 | Clasificación:  A1
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Estudios en psicología', 'Código del Grupo: COL0180234 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Estudios florísticos', 'Código del Grupo: COL0004639 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Estudios geológicos especiales', 'Código del Grupo: COL0172401 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Servicio Geológico Colombiano
Municipio: Medellín'),
(12,'Estudios interculturales y decoloniales', 'Código del Grupo: COL0170209 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estudios interdisciplinares en historia general', 'Código del Grupo: COL0133931 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estudios interdisciplinarios sobre educación -esined-', 'Código del Grupo: COL0122796 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Estudios internacionales - Tdea', 'Código del Grupo: COL0191407 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Estudios internacionales: derecho, economía, política y relaciones internacionales', 'Código del Grupo: COL0112753 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estudios literarios', 'Código del Grupo: COL0004685 | Clasificación:  A1
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estudios musicales', 'Código del Grupo: COL0058209 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Estudios musicales y educación musical', 'Código del Grupo: COL0075266 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Estudios organizacionales Unac', 'Código del Grupo: COL0125241 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Estudios políticos UPB', 'Código del Grupo: COL0021649 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Estudios políticos UdeA', 'Código del Grupo: COL0004658 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Estudios sobre juventud', 'Código del Grupo: COL0031225 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ética', 'Código del Grupo: COL0081414 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Etices (salud, humanismo y bioética)', 'Código del Grupo: COL0118256 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Etnohistoria y estudios sobre américas negras', 'Código del Grupo: COL0037882 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Etnosaludafro', 'Código del Grupo: COL0198604 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Fundacion de Comunidaddes Negras Afrocolombianas Raizales y Palenqueras ETN
Municipio: Medellín'),
(12,'E-virtual', 'Código del Grupo: COL0036079 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Evo-devo en plantas', 'Código del Grupo: COL0170292 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Facea', 'Código del Grupo: COL0001576 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Familia, desarrollo y calidad de vida', 'Código del Grupo: COL0044949 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Sociología
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Farmacodependencia y otras adicciones', 'Código del Grupo: COL0004872 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Farmacoepidemiologia y gestión de riesgo', 'Código del Grupo: COL0199782 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Helpharma
Municipio: Medellín'),
(12,'Fenómenos de superficie - Michael Polanyi', 'Código del Grupo: COL0163897 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Fes', 'Código del Grupo: COL0148237 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Derecho
Institución aval: Politécnico Arzobispo Salazar Y Herrera
Municipio: Medellín'),
(12,'Filosofía política', 'Código del Grupo: COL0005083 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Finanzas y sostenibilidad', 'Código del Grupo: COL0203393 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Física básica y aplicada', 'Código del Grupo: COL0034488 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Física radiológica', 'Código del Grupo: COL0005314 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Fisicoquímica orgánica', 'Código del Grupo: COL0005118 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Fisioter: importancia del movimiento en el desarrollo humano', 'Código del Grupo: COL0032409 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Fundación Universitaria Maria Cano
Municipio: Medellín'),
(12,'Fitobiol', 'Código del Grupo: COL0177059 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Fitotecnia tropical', 'Código del Grupo: COL0018179 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid | Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Fonotec', 'Código del Grupo: COL0061921 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Otras Ciencias Médicas
Institución aval: Fundación Universitaria Maria Cano
Municipio: Medellín'),
(12,'Fotónica y opto-electrónica', 'Código del Grupo: COL0019006 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Fundamentos y enseñanza de la física y los sistemas dinámicos', 'Código del Grupo: COL0139559 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias físicas
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'G-3in', 'Código del Grupo: COL0078455 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Gacipe (grupo de investigación en automatización, comunicaciones industriales , pedagogía y energías alternativas)', 'Código del Grupo: COL0113017 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Medellín'),
(12,'Gaf (grupo de alimentos funcionales)', 'Código del Grupo: COL0028471 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Gama', 'Código del Grupo: COL0036849 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Games', 'Código del Grupo: COL0102579 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institucion Universitaria Escolme
Municipio: Medellín'),
(12,'Gea', 'Código del Grupo: COL0016452 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Institución Universitaria Salazar y Herrera
Municipio: Medellín'),
(12,'Gebiomic (genética y bioquímica de microorganismos)', 'Código del Grupo: COL0014064 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Geincro', 'Código del Grupo: COL0174836 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Fundacion Universitaria San Martin
Municipio: Sabaneta'),
(12,'Genética médica', 'Código del Grupo: COL0006732 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Genética molecular (genmol)', 'Código del Grupo: COL0006723 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Genética, regeneración y cáncer', 'Código del Grupo: COL0006769 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Genomaces', 'Código del Grupo: COL0198239 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Geoinformatica aplicada', 'Código del Grupo: COL0191363 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Geolimna', 'Código del Grupo: COL0135041 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Geología ambiental e ingeniería sísmica', 'Código del Grupo: COL0006803 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Georesearch international-geor', 'Código del Grupo: COL0163233 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gepar-grupo de electrónica de potencia, automatización y robótica', 'Código del Grupo: COL0039045 | Clasificación:  B
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingenierías eléctrica, electrónica e informática
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Gepidh (grupo de estudios en pedagogía, infancia y desarrollo humano)', 'Código del Grupo: COL0019829 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gesta - emprendimiento, finanzas y gestión organizacional', 'Código del Grupo: COL0127353 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gestas', 'Código del Grupo: COL0072129 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Gestión de la calidad', 'Código del Grupo: COL0058541 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gestión de la construcción', 'Código del Grupo: COL0023509 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Gestión de la innovación y el desarrollo tecnológico', 'Código del Grupo: COL0110481 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Tecknowledge | Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Medellín'),
(12,'Gestión de la tecnología y la innovación (gti.upb)', 'Código del Grupo: COL0019893 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Gestión de servicios de alimentación y nutrición a colectividades - gesanc', 'Código del Grupo: COL0142144 | Clasificación:  C
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Ciencias de la salud
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Gestión y políticas de salud', 'Código del Grupo: COL0007014 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gestión y políticas públicas territoriales -gppt-', 'Código del Grupo: COL0078286 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias políticas
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Ghygam', 'Código del Grupo: COL0005519 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Giae (grupo de investigación acción y evaluación en lenguas extranjeras)', 'Código del Grupo: COL0009037 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Giat - grupo de investigación de aplicaciones en telecomunicaciones', 'Código del Grupo: COL0097432 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Giaz', 'Código del Grupo: COL0175412 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Otras Ciencias Agrícolas
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Gibpsicos', 'Código del Grupo: COL0002303 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Gicea', 'Código del Grupo: COL0086009 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Fundación Universitaria Autónoma de las Américas
Municipio: Medellín'),
(12,'Gidep. Grupo interdisciplinario de estudios pedagógicos', 'Código del Grupo: COL0028524 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Gidia: grupo de investigación y desarrollo en inteligencia artificial', 'Código del Grupo: COL0030139 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Giditic: grupo  i+d+i  en tecnologías  de  la  información y las  comunicaciones', 'Código del Grupo: COL0013281 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Gidoca', 'Código del Grupo: COL0149529 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Otras Ciencias Agrícolas
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Santa Fé de Antioquia'),
(12,'Giem - unac - grupo de investigación en emprendimiento - empresarismo y marketing unac', 'Código del Grupo: COL0026009 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Gieri: grupo de investigación en enfermedades respiratorias e infecciosas', 'Código del Grupo: COL0130831 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: I.P.S. Universitaria | Universidad de Antioquia
Municipio: Medellín'),
(12,'Gif (grupo de investigación en filosofía)', 'Código del Grupo: COL0004943 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Gifi - grupo de investigación en finanzas de la Udea', 'Código del Grupo: COL0154341 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gificur (grupo de estudio de filosofía política contemporánea)', 'Código del Grupo: COL0090825 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Gigat-grupo de investigación en gerencia y aplicación de la ciencia y la tecnología', 'Código del Grupo: COL0131024 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Medellín'),
(12,'Giiesen grupo de investigación interdisciplinaria en educación para la salud y educación nutricional', 'Código del Grupo: COL0103333 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gimacyr - grupo de investigación en materiales y recubrimientos cerámicos', 'Código del Grupo: COL0083393 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gimsc (grupo de investigación en modelamiento y simulación computacional)', 'Código del Grupo: COL0005646 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Gimu (grupo de investigación en ingeniería multidisciplinar)', 'Código del Grupo: COL0040369 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Ginews', 'Código del Grupo: COL0102149 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: New Stetic S.A.
Municipio: Guarne'),
(12,'Ginver', 'Código del Grupo: COL0121592 | Clasificación:  B
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Ginvestap', 'Código del Grupo: COL0031869 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingenierías eléctrica, electrónica e informática
Institución aval: Instituto Tecnológico Metropolitano De Medellín - I.T.M.
Municipio: Medellín'),
(12,'Giom', 'Código del Grupo: COL0047889 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Gipab (grupo de investigación en procesos ambientales)', 'Código del Grupo: COL0005593 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Gipeci', 'Código del Grupo: COL0098304 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias políticas
Institución aval: Corporación Universitaria Americana
Municipio: Medellín'),
(12,'Gisca', 'Código del Grupo: COL0122615 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Fundación Universitaria Autónoma de las Américas
Municipio: Medellín'),
(12,'Gismac. Grupo de investigación en sistemas marinos y costeros', 'Código del Grupo: COL0023849 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad De Antioquia - Sede Turbo
Municipio: Turbo'),
(12,'Gitima', 'Código del Grupo: COL0191452 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Glopri-globalización del derecho privado', 'Código del Grupo: COL0181849 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Gobierno y asuntos públicos', 'Código del Grupo: COL0066614 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gobierno, instituciones y transparencia - git', 'Código del Grupo: COL0142206 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Goras', 'Código del Grupo: COL0044798 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Gpc - gerencia, productividad y competitividad', 'Código del Grupo: COL0023401 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Escuela de Ingeniería de Antioqua
Municipio: Envigado'),
(12,'Greco grupo de estudios en comunicación', 'Código del Grupo: COL0121028 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grial', 'Código del Grupo: COL0037701 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Grid - grupo de investigación dermatológica', 'Código del Grupo: COL0050839 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: I.P.S. Universitaria | Universidad de Antioquia | Fundación Hospitalaria San Vicente De Paúl | Corporación para Investigaciones Biológicas | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Gridic - grupo de investigación de ingeniería civil', 'Código del Grupo: COL0019347 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingeniería civil
Institución aval: Politecnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grinbio grupo de investigación en biodiversidad, biotecnología y bioingeniería', 'Código del Grupo: COL0120549 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grindis grupo de investigación en diálogos diversos', 'Código del Grupo: COL0166834 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Grinpab', 'Código del Grupo: COL0087712 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Institución Educativa Antonio José Bernal Londoño
Municipio: Medellín'),
(12,'Grinsoft "grupo de investigación en software"', 'Código del Grupo: COL0074089 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grintec', 'Código del Grupo: COL0156515 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Fundación Universitaria Maria Cano
Municipio: Medellín'),
(12,'Gripe: grupo investigador de problemas en enfermedades infecciosas', 'Código del Grupo: COL0005744 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigaciones jurídicas universidad de medellín', 'Código del Grupo: COL0007444 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo ingeniería de tejidos y terapias celulares', 'Código del Grupo: COL0027213 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: I.P.S. Universitaria | Universidad de Antioquia | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo académico de epidemiología clínica', 'Código del Grupo: COL0007121 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo cardiovascular de investigaciones', 'Código del Grupo: COL0007186 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina clínica
Institución aval: Clínica Cardiovascular Santa María - Cvsm
Municipio: Medellín'),
(12,'Grupo ceres - agroindustria & ingeniería', 'Código del Grupo: COL0143249 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo colombiano de investigación en enfermedades neuromusculares -gien-', 'Código del Grupo: COL0149379 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Fundación Instituto Neurológico De Colombia | Medicina Computacional S.A.S. | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de alergología clínica y experimental (gace)', 'Código del Grupo: COL0059567 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: I.P.S. Universitaria | Universidad de Antioquia | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de artes performativas', 'Código del Grupo: COL0087383 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y literatura
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de automática de la universidad nacional gaunal', 'Código del Grupo: COL0007758 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de automática y diseño a+d', 'Código del Grupo: COL0007767 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de biofísica', 'Código del Grupo: COL0022511 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de biofísica-Udea', 'Código del Grupo: COL0076414 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de biomateriales avanzados y medicina regenerativa, bamr', 'Código del Grupo: COL0144399 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de bioquímica estructural de macromoléculas', 'Código del Grupo: COL0156275 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de biotransformación', 'Código del Grupo: COL0066991 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de coloides', 'Código del Grupo: COL0007874 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de desarrollo y diseño de procesos', 'Código del Grupo: COL0037569 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de dinámica cardiovascular', 'Código del Grupo: COL0007945 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de diseño mecánico', 'Código del Grupo: COL0086448 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de diseño mecánico computacional', 'Código del Grupo: COL0075856 | Clasificación:  A
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingeniería mecánica
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de econometría aplicada -gea-', 'Código del Grupo: COL0054767 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de economía y medio ambiente', 'Código del Grupo: COL0007963 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de educación en ciencias experimentales y matemáticas - gecem', 'Código del Grupo: COL0008002 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de energía alternativa', 'Código del Grupo: COL0008058 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de energía y termodinámica', 'Código del Grupo: COL0008076 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de entomología médica de la universidad de Antioquia', 'Código del Grupo: COL0008109 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de entomología universidad de Antioquia', 'Código del Grupo: COL0066697 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estabilidad de medicamentos, cosméticos y alimentos, gemca', 'Código del Grupo: COL0035117 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estado sólido', 'Código del Grupo: COL0008138 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estudio e investigaciones biofarmacéuticas', 'Código del Grupo: COL0049453 | Clasificación:  B
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina básica
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de estudios botánicos', 'Código del Grupo: COL0008227 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estudios de literatura y cultura intelectual latinoamericana', 'Código del Grupo: COL0064075 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estudios e investigaciones educativas y pedagógicas g.e.i.e.p', 'Código del Grupo: COL0116189 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Grupo de estudios en arquitectura y urbanística', 'Código del Grupo: COL0027797 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de estudios en ciencia política y administración publica', 'Código del Grupo: COL0051934 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de estudios en economía y empresa', 'Código del Grupo: COL0008343 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de estudios en geología y geofísica - egeo', 'Código del Grupo: COL0150558 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de estudios en mantenimiento - gemi', 'Código del Grupo: COL0008325 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de estudios en mercadeo', 'Código del Grupo: COL0011169 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de estudios en política y guerra', 'Código del Grupo: COL0056144 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias políticas
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de estudios en psicología aplicada y sociedad -  pays', 'Código del Grupo: COL0002994 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Institución Universitaria de Envigado
Municipio: Envigado'),
(12,'Grupo de estudios en turismo "get"', 'Código del Grupo: COL0141209 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de estudios estéticos', 'Código del Grupo: COL0008263 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de estudios jurídicos', 'Código del Grupo: COL0056986 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Grupo de estudios lingüísticos regionales', 'Código del Grupo: COL0008272 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estudios regionales', 'Código del Grupo: COL0029343 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estudios sociolingüísticos', 'Código del Grupo: COL0002831 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de estudios tectónicos get', 'Código del Grupo: COL0068637 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de fenomenología de interacciones fundamentales', 'Código del Grupo: COL0008423 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de física atómica y molecular', 'Código del Grupo: COL0008441 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de física teórica', 'Código del Grupo: COL0024749 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias físicas
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de física teórica y matemática aplicada', 'Código del Grupo: COL0190188 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de física y astrofísica computacional (facom)', 'Código del Grupo: COL0038262 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de gastrohepatología', 'Código del Grupo: COL0024159 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de geofísica y ciencias de la computación -ggc3', 'Código del Grupo: COL0095447 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Grupo de geotecnia', 'Código del Grupo: COL0036301 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de gestión del desarrollo agrario - gestiagro', 'Código del Grupo: COL0108169 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grupo de ictiología', 'Código del Grupo: COL0078704 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias biológicas
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de ingeniería electroquímica - griequi', 'Código del Grupo: COL0027939 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de ingeniería y gestión ambiental giga', 'Código del Grupo: COL0008619 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de inmunología celular e inmunogenética', 'Código del Grupo: COL0008639 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de innovación en matemáticas y nuevas tecnologías para la educación - gnomon -', 'Código del Grupo: COL0085585 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Grupo de investigación acuícola (gia)', 'Código del Grupo: COL0006625 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grupo de investigación ambiente, hábitat y sostenibilidad', 'Código del Grupo: COL0095975 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Institución Universitaria Colegio Mayor de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación aplicada a la industria, al textil y a la química (giaiteq)', 'Código del Grupo: COL0151199 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación aplicada en polímeros', 'Código del Grupo: COL0009055 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: INSTITUTO DE CAPACITACIÓN E INVESTIGACIÓN DEL PLÁSTICO Y DEL CAUCHO - ICIPC
Municipio: Medellín'),
(12,'Grupo de investigación arte y cultura', 'Código del Grupo: COL0166923 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Corporación Alma Arte y Acción
Municipio: Rionegro'),
(12,'Grupo de investigación audiovisual -interdis-', 'Código del Grupo: COL0051469 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y literatura
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de investigación biohacking y biología sintética', 'Código del Grupo: COL0196609 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Biohacking Colombia
Municipio: Medellín'),
(12,'Grupo de investigación centro de historia de bello', 'Código del Grupo: COL0052388 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y arqueología
Institución aval: Centro De Historia De Bello
Municipio: Bello'),
(12,'Grupo de investigación ciencia, tecnología e innovación en salud (citeisa)', 'Código del Grupo: COL0150772 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación clínica en enfermedades del niño y del adolescente - pediaciencias', 'Código del Grupo: COL0058784 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación cultura somática', 'Código del Grupo: COL0020286 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación de administración en salud ocupacional y contaduría pública - asocop', 'Código del Grupo: COL0205594 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Corporación Universitaria Minuto de Dios - UNIMINUTO
Municipio: Bello'),
(12,'Grupo de investigación de estudios en diseño', 'Código del Grupo: COL0031519 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación de estudios internacionales', 'Código del Grupo: COL0070484 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institución Universitaria Esumer
Municipio: Medellín'),
(12,'Grupo de investigación de labio y paladar hendido fisiología oral y crecimiento craneofacial - ceslph', 'Código del Grupo: COL0023724 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Sabaneta'),
(12,'Grupo de investigación de operaciones de la universidad nacional de Colombia: ungido', 'Código del Grupo: COL0134053 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación de sanidad vegetal', 'Código del Grupo: COL0009322 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Grupo de investigación de tecnología en regencia de farmacia', 'Código del Grupo: COL0135121 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación de vértigo y alteraciones del equilibrio', 'Código del Grupo: COL0157817 | Clasificación:  B
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina clínica
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación del cidet', 'Código del Grupo: COL0043959 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: CIDET
Municipio: Medellín'),
(12,'Grupo de investigación desarrollo psicosocial', 'Código del Grupo: COL0204013 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Grupo de investigación diseño de vestuario y textiles', 'Código del Grupo: COL0103404 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación e innovación biomédica', 'Código del Grupo: COL0056476 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Grupo de investigación e innovación en energía - giien', 'Código del Grupo: COL0119422 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Institución Universitaria Pascual Bravo
Municipio: Medellín'),
(12,'Grupo de investigación e innovación en formulaciones químicas', 'Código del Grupo: COL0177687 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Escuela de Ingeniería de Antioqua
Municipio: Envigado'),
(12,'Grupo de investigación edusalud', 'Código del Grupo: COL0104528 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación emat', 'Código del Grupo: COL0051219 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación empresarial y turístico', 'Código del Grupo: COL0048259 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Institución Universitaria Colegio Mayor de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en  dolor y cuidado paliativo', 'Código del Grupo: COL0008728 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en  física teórica, aplicada y didáctica - gritad', 'Código del Grupo: COL0104045 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en actividad física para la salud - afis', 'Código del Grupo: COL0168113 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en agrociencias, biodiversidad y territorio gamma', 'Código del Grupo: COL0006779 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en agua y medio ambiente - ama', 'Código del Grupo: COL0089879 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias de la tierra y medioambientales
Institución aval: Corporación Centro de Ciencia y Tecnología de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en alimentos saludables', 'Código del Grupo: COL0048419 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Otras ingenierías y tecnologías
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en análisis económico UPB (gae-upb)', 'Código del Grupo: COL0029844 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en análisis sensorial', 'Código del Grupo: COL0106039 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en áreas jurídicas e interdisciplinares', 'Código del Grupo: COL0185938 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: CORPORACIÓN UNIVERSITARIA U DE COLOMBIA
Municipio: Medellín'),
(12,'Grupo de investigación en arte', 'Código del Grupo: COL0051049 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en biodiversidad tropical', 'Código del Grupo: COL0052501 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Fundación Jardín Botánico Joaquín Antonio Uribe
Municipio: Medellín'),
(12,'Grupo de investigación en bioingeniería (gib) ces - eafit', 'Código del Grupo: COL0009458 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: Universidad EAFIT | Universidad CES
Municipio: Medellín'),
(12,'Grupo de investigación en bioinstrumentación e ingeniería clínica:  gibic', 'Código del Grupo: COL0054963 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en biomateriales', 'Código del Grupo: COL0055049 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en biomecánica e ingeniería de rehabilitación (gibir)', 'Código del Grupo: COL0150816 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en biosuperficies', 'Código del Grupo: COL0204695 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Nanotecnología
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en biotecnología animal (giba)', 'Código del Grupo: COL0064001 | Clasificación:  A
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid | Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en calidad del agua y modelación hídrica (gicamh)', 'Código del Grupo: COL0137289 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigación en cáncer idc', 'Código del Grupo: COL0130878 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Instituto de Cancerología S.A.
Municipio: Medellín'),
(12,'Grupo de investigación en catálisis ambiental y energías renovables', 'Código del Grupo: COL0037846 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grupo de investigación en catálisis y nanomateriales', 'Código del Grupo: COL0128735 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en ciencias agrarias -grica-', 'Código del Grupo: COL0009556 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en ciencias básicas lasallista', 'Código del Grupo: COL0165194 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Otras ciencias naturales
Institución aval: Corporación Universitaria Lasallista
Municipio: Caldas'),
(12,'Grupo de investigación en ciencias económicas y administrativas gicea', 'Código del Grupo: COL0110178 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Grupo de investigación en ciencias electrónicas e informáticas - gicei', 'Código del Grupo: COL0114524 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Nanotecnología
Institución aval: Institución Universitaria Pascual Bravo
Municipio: Medellín'),
(12,'Grupo de investigación en ciencias empresariales - gice', 'Código del Grupo: COL0030049 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institución Universitaria de Envigado
Municipio: Envigado'),
(12,'Grupo de investigación en ciencias farmacéuticas icif-ces', 'Código del Grupo: COL0154083 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Grupo de investigación en ciencias forestales', 'Código del Grupo: COL0003258 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias agrícolas
Subárea de Conocimiento: Agricultura, silvicultura y pesca
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de investigación en ciencias y tecnología de alimentos -gicta-', 'Código del Grupo: COL0019641 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en cirugía plástica Universidad de Antioquia', 'Código del Grupo: COL0038306 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en compuestos funcionales', 'Código del Grupo: COL0046119 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en comunicación -gic', 'Código del Grupo: COL0025003 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Periodismo y comunicaciones
Institución aval: Politecnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grupo de investigación en comunicación urbana', 'Código del Grupo: COL0006339 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en conservación y manejo de agroecosistemas', 'Código del Grupo: COL0045777 | Clasificación:  B
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid | Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en construcción', 'Código del Grupo: COL0051068 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en contabilidad y organizaciones gicor', 'Código del Grupo: COL0090208 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Grupo de investigación en cuidado', 'Código del Grupo: COL0024356 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en cuidado crítico', 'Código del Grupo: COL0008746 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana | Clínica Universitaria Bolivariana - Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en cultura y espiritualidad (ince)', 'Código del Grupo: COL0077126 | Clasificación:  D
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad Pontificia Bolivariana - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en derecho - gride', 'Código del Grupo: COL0109613 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Corporación Universitaria Lasallista
Municipio: Caldas'),
(12,'Grupo de investigación en desarrollo de las empresas (gidemp)', 'Código del Grupo: COL0139399 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporación Universitaria de Sabaneta
Municipio: Sabaneta'),
(12,'Grupo de investigación en diseño gráfico - gidg', 'Código del Grupo: COL0066893 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en ecología y silvicultura de especies forestales tropicales', 'Código del Grupo: COL0048349 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en economía aplicada (gea)', 'Código del Grupo: COL0028794 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigación en economía solidaria-gies', 'Código del Grupo: COL0106451 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Universidad Cooperativa De Colombia
Municipio: Medellín'),
(12,'Grupo de investigación en ecosistemas y cambio global', 'Código del Grupo: COL0028257 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Corporación De Centro De Investigación Carbono & Bosques
Municipio: Medellín'),
(12,'Grupo de investigación en educación superior. Ces.', 'Código del Grupo: COL0028489 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Grupo de investigación en educación y docencia', 'Código del Grupo: COL0022306 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Grupo de investigación en educación, sociedad y paz', 'Código del Grupo: COL0025059 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigación en empresa y territorio (giemto)', 'Código del Grupo: COL0166404 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Corporación Universitaria de Sabaneta
Municipio: Sabaneta'),
(12,'Grupo de investigación en energía - grinen', 'Código del Grupo: COL0140838 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigación en especialidades médico-quirúrgicas ces', 'Código del Grupo: COL0204873 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES | Clínica CES
Municipio: Medellín'),
(12,'Grupo de investigación en estadística universidad nacional de Colombia, sede Medellín', 'Código del Grupo: COL0009716 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en estudios clásicos y semíticos', 'Código del Grupo: COL0019561 | Clasificación:  D
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad Pontificia Bolivariana - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en estudios internacionales', 'Código del Grupo: COL0041553 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de investigación en estudios sobre desarrollo local y gestión territorial', 'Código del Grupo: COL0112038 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Institución Universitaria Colegio Mayor de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en ética y bioética (gieb)', 'Código del Grupo: COL0119879 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en familia', 'Código del Grupo: COL0009743 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en farmacología y toxicología " infarto"', 'Código del Grupo: COL0039902 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en filosofía y teología crítica', 'Código del Grupo: COL0011287 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Grupo de investigación en finanzas y banca', 'Código del Grupo: COL0006035 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de investigación en fisiología y bioquímica - physis', 'Código del Grupo: COL0007328 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en género, subjetividad y sociedad', 'Código del Grupo: COL0110839 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Sociología
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en genómica y conservación (geneco)', 'Código del Grupo: COL0200945 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Corporación para Investigaciones Biológicas
Municipio: Medellín'),
(12,'Grupo de investigación en geomecánica aplicada', 'Código del Grupo: COL0026557 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en georrecursos, minería y medio ambiente. Gemma', 'Código del Grupo: COL0011198 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en gestión cultural', 'Código del Grupo: COL0044644 | Clasificación:  D
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en gestión de la comunicación', 'Código del Grupo: COL0067489 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en gestión de producción y logística', 'Código del Grupo: COL0025371 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de investigación en gestión empresarial', 'Código del Grupo: COL0057221 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Grupo de investigación en gestión organizacional- gestor Udea.', 'Código del Grupo: COL0006358 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en gestión y desarrollo gdo', 'Código del Grupo: COL0097218 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Grupo de investigación en gestión y modelación ambiental (gaia)', 'Código del Grupo: COL0009832 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en gestión, evaluación e innovación educativa - educere - cta', 'Código del Grupo: COL0160821 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: CORPORACIÓN CENTRO DE CIENCIA Y TECNOLOGÍA DE ANTIOQUIA
Municipio: Medellín'),
(12,'Grupo de investigación en ginecología y obstetricia', 'Código del Grupo: COL0007032 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad Pontificia Bolivariana | Clínica Universitaria Bolivariana - Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en historia social', 'Código del Grupo: COL0009879 | Clasificación:  A1
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad Nacional de Colombia - Sede Medellín | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en información, conocimiento y sociedad', 'Código del Grupo: COL0074901 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en informática educativa - guiame', 'Código del Grupo: COL0118597 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en infraestructura - gii', 'Código del Grupo: COL0155367 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería aeroespacial', 'Código del Grupo: COL0026216 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería aplicada gi2a', 'Código del Grupo: COL0052815 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería biomédica (gibec)', 'Código del Grupo: COL0036679 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: UNIVERSIDAD EIA | Universidad CES
Municipio: Envigado'),
(12,'Grupo de investigación en ingeniería de diseño (grid)', 'Código del Grupo: COL0055862 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería de procesos, energía y medio ambiente', 'Código del Grupo: COL0100485 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Hatch Indisa S.A.S.
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería de software del Tecnológico de Antioquia-giista', 'Código del Grupo: COL0064404 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería financiera (ginif)', 'Código del Grupo: COL0029559 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería financiera y gestión empresarial. (gifig)', 'Código del Grupo: COL0062169 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de investigación en ingeniería ingeniar', 'Código del Grupo: COL0129769 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Grupo de investigación en innovación y agroindustria (giia)', 'Código del Grupo: COL0106783 | Clasificación:  B
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Rionegro'),
(12,'Grupo de investigación en instrumentación, control automático y robótica (icaro)', 'Código del Grupo: COL0020535 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grupo de investigación en inteligencia computacional', 'Código del Grupo: COL0040565 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en inteligencia computacional y automática. Giica', 'Código del Grupo: COL0048017 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Escuela de Ingeniería de Antioqua
Municipio: Envigado'),
(12,'Grupo de investigación en intervención social', 'Código del Grupo: COL0098402 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en logística y administración de cadena de suministro (gilacs)', 'Código del Grupo: COL0176939 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Corporación Escuela Superior en Administración de Cadena de Suministro
Municipio: Medellín'),
(12,'Grupo de investigación en matemáticas - gmat', 'Código del Grupo: COL0047996 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en matemáticas universidad nacional de Colombia sede Medellín', 'Código del Grupo: COL0006062 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en materiales de ingeniería (gme)', 'Código del Grupo: COL0064861 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de investigación en materiales y procesos alternativos. Mapa', 'Código del Grupo: COL0065555 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Escuela de Ingeniería de Antioqua
Municipio: Envigado'),
(12,'Grupo de investigación en materiales y sistemas energéticos tesla', 'Código del Grupo: COL0124235 | Clasificación:  B
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingenierías eléctrica, electrónica e informática
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en medicina aplicada a la  actividad física y el deporte -grinmade-', 'Código del Grupo: COL0070223 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en medicina interna', 'Código del Grupo: COL0020553 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana | Clínica Universitaria Bolivariana - Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en medicina perioperatoria (grimpa)', 'Código del Grupo: COL0094314 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Biotecnología en Salud
Institución aval: I.P.S. Universitaria | Universidad de Antioquia | Fundación Hospitalaria San Vicente De Paúl | Universidad de Antioquia - Sede Carmen de Viboral | Fundacion Hospital San Vicente de Paul - Rionegro
Municipio: Medellín'),
(12,'Grupo de investigación en medicina veterinaria givet', 'Código del Grupo: COL0101348 | Clasificación:  B
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Grupo de investigación en microbiología básica y aplicada-microba', 'Código del Grupo: COL0126131 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en microbiología veterinaria', 'Código del Grupo: COL0021461 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias biológicas
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en microelectrónica', 'Código del Grupo: COL0010029 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en nanotecnología y materiales', 'Código del Grupo: COL0181689 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Nanotecnología
Institución aval: Nanomat S.A.S.
Municipio: Medellín'),
(12,'Grupo de investigación en odontología del niño y ortodoncia', 'Código del Grupo: COL0090119 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en oftalmología ces', 'Código del Grupo: COL0066919 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Grupo de investigación en oftalmología universidad pontificia bolivariana -Medellín', 'Código del Grupo: COL0024481 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en organización, ambiente y sociedad "likapaay"', 'Código del Grupo: COL0032483 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Grupo de investigación en organizaciones kabai', 'Código del Grupo: COL0145745 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Grupo de investigación en patobiología - quirón', 'Código del Grupo: COL0092964 | Clasificación:  B
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en patología clínica', 'Código del Grupo: COL0148578 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina clínica
Institución aval: Laboratorio Clínico Hematológico S.A.
Municipio: Medellín'),
(12,'Grupo de investigación en patología oral, periodoncia y cirugía alvéolo-dentaria-popcad', 'Código del Grupo: COL0010074 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en procesos de construcción y transformación social - trayectos', 'Código del Grupo: COL0201512 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Grupo de investigación en procesos dinámicos-kalman', 'Código del Grupo: COL0101221 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en producción, desarrollo y transformación agropecuaria', 'Código del Grupo: COL0071267 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Grupo de investigación en productividad del valor agregado - productivamente', 'Código del Grupo: COL0160839 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Otras ingenierías y tecnologías
Institución aval: Corporación Centro de Ciencia y Tecnología de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en psicología aplicada', 'Código del Grupo: COL0086429 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Corporacion Universitaria Lasallista
Municipio: Caldas'),
(12,'Grupo de investigación en psicología cognitiva', 'Código del Grupo: COL0011447 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de los Andes | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en psicología: sujeto, sociedad y trabajo', 'Código del Grupo: COL0010199 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en psiquiatría de enlace', 'Código del Grupo: COL0049524 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en psiquiatría gipsi', 'Código del Grupo: COL0029147 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en remediación ambiental y biocatálisis', 'Código del Grupo: COL0125116 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en respuestas estratégicas', 'Código del Grupo: COL0151699 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institución Universitaria Salazar y Herrera
Municipio: Medellín'),
(12,'Grupo de investigación en salud del adulto mayor (gisam)', 'Código del Grupo: COL0046086 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Grupo de investigación en salud familiar y comunitaria', 'Código del Grupo: COL0184879 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Grupo de investigación en salud pública', 'Código del Grupo: COL0053071 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad Pontificia Bolivariana | Clínica Universitaria Bolivariana - Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en salud y comunidad', 'Código del Grupo: COL0122141 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Fundación Universitaria Autónoma de las Américas
Municipio: Medellín'),
(12,'Grupo de investigación en seguridad de la información en tecnologías emergentes - csiete labs', 'Código del Grupo: COL0164633 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: CSIETE
Municipio: Medellín'),
(12,'Grupo de investigación en semiología saussuriana - semsa', 'Código del Grupo: COL0087589 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación, en síntesis, reactividad y transformación de compuestos orgánicos, sirytcor', 'Código del Grupo: COL0069788 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en sistemas agroambientales sostenibles', 'Código del Grupo: COL0144489 | Clasificación:  D
Área de Conocimiento: Ciencias agrícolas
Subárea de Conocimiento: Agricultura, silvicultura y pesca
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en sistemas aplicados a la industria', 'Código del Grupo: COL0099876 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en sistemas e informática', 'Código del Grupo: COL0038923 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Institución Universitaria de Envigado
Municipio: Envigado'),
(12,'Grupo de investigación en sistemática molecular', 'Código del Grupo: COL0010332 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en sostenibilidad gis', 'Código del Grupo: COL0141871 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Otras ciencias sociales
Institución aval: Universidad de Medellín - Udem
Municipio: Medellín'),
(12,'Grupo de investigación en sustancias bioactivas -gisb-', 'Código del Grupo: COL0010359 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en tecnologías aplicadas - gita', 'Código del Grupo: COL0030765 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación en telecomunicaciones aplicadas - gita', 'Código del Grupo: COL0044448 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en temas agroambientales - integra', 'Código del Grupo: COL0063793 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Otras Ciencias Agrícolas
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en teología, religión y cultura', 'Código del Grupo: COL0008899 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en terminología y traducción - gitt', 'Código del Grupo: COL0010388 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y literatura
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en trabajo social gits', 'Código del Grupo: COL0145299 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en traducción y nuevas tecnologías (tnt)', 'Código del Grupo: COL0074652 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en traductología', 'Código del Grupo: COL0011456 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en transmisión y distribución de ener', 'Código del Grupo: COL0010412 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación en urgencias y emergencias (giure)', 'Código del Grupo: COL0187834 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación en violencia urbana', 'Código del Grupo: COL0010439 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Ciencias de la salud
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de investigación en vulcanología - giv', 'Código del Grupo: COL0030291 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: UNIVERSIDAD NACIONAL DE COLOMBIA SEDE BOGOTA
Municipio: Medellín'),
(12,'Grupo de investigación enseñanza y aprendizaje de lenguas', 'Código del Grupo: COL0009028 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de investigación epilión', 'Código del Grupo: COL0123069 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación industrial en diseño de materiales a partir de minerales y procesos (idimap)', 'Código del Grupo: COL0131418 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: SUMICOL S.A.S. | Minerales Industriales S.A.
Municipio: Sabaneta'),
(12,'Grupo de investigación instituto de alta tecnología médica de Antioquia - iatm', 'Código del Grupo: COL0089279 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: DIAGNOSTICO & ASISTENCIA MEDICA S.A INSTITUCION PRESTADORA DE SERVICIOS DE SALUD - DINAMICA I P S | FUNDACIÓN INSTITUTO DE ALTA TECNOLOGÍA MÉDICA DE ANTIOQUIA - IATM
Municipio: Medellín'),
(12,'Grupo de investigación interdisciplinaria explora', 'Código del Grupo: COL0074151 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Corporación Parque Explora
Municipio: Medellín'),
(12,'Grupo de investigación la práctica de enfermería en el contexto social', 'Código del Grupo: COL0146822 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación la salada', 'Código del Grupo: COL0085647 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Caldas'),
(12,'Grupo de investigación materiales con impacto (mat&mpac)', 'Código del Grupo: COL0181394 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigación medio ambiente y sociedad', 'Código del Grupo: COL0009206 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación netux lab', 'Código del Grupo: COL0187941 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: NETUX S.A.S.
Municipio: Medellín'),
(12,'Grupo de investigación para las prácticas artísticas en contexto', 'Código del Grupo: COL0155296 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Escuela Superior Tecnológica de Artes Débora Arango
Municipio: Medellín'),
(12,'Grupo de investigación psicología, sociedad y subjetividades.', 'Código del Grupo: COL0045946 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación recreando a Urabá - girau', 'Código del Grupo: COL0189955 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Fundación de Estudios Superiores Universitarios de Urabá Antonio Roldán Betancur
Municipio: Apartadó'),
(12,'Grupo de investigación ser', 'Código del Grupo: COL0009251 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Grupo de investigación sobre conflictos, violencias y seguridad humana', 'Código del Grupo: COL0017119 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación sobre estudios críticos', 'Código del Grupo: COL0162601 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación sobre formación y antropología pedagógica e histórica -formaph-', 'Código del Grupo: COL0011483 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación sobre nuevos materiales', 'Código del Grupo: COL0006419 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación socio-jurídico udec', 'Código del Grupo: COL0170989 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Derecho
Institución aval: CORPORACIÓN UNIVERSITARIA U DE COLOMBIA
Municipio: Medellín'),
(12,'Grupo de investigación teológico pastoral en matrimonio y familia domus', 'Código del Grupo: COL0071703 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad Pontificia Bolivariana - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de investigación y desarrollo de aplicaciones en tecnologías de la información y la comunicación (gidatic)', 'Código del Grupo: COL0010628 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigación y estudios del inglés y el español', 'Código del Grupo: COL0096059 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Grupo de investigación y gestión sobre patrimonio', 'Código del Grupo: COL0055129 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Sociología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigación, innovación y desarrollo en salud ideas biomédicas(gi^3+db)', 'Código del Grupo: COL0200909 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Ideas Biomédicas S.A.S
Municipio: Medellín'),
(12,'Grupo de investigación, innovación y desarrollo para la subregión del urabá- abibe', 'Código del Grupo: COL0146626 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Apartadó'),
(12,'Grupo de investigaciones agroindustriales -grain-', 'Código del Grupo: COL0001001 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Otras Ciencias Agrícolas
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigaciones ambientales', 'Código del Grupo: COL0006222 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigaciones biomédicas uniremington', 'Código del Grupo: COL0063489 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Grupo de investigaciones contables y gestión pública', 'Código del Grupo: COL0018878 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigaciones económicas ginveco', 'Código del Grupo: COL0087561 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Grupo de investigaciones en bioingeniería', 'Código del Grupo: COL0008906 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigaciones en derecho', 'Código del Grupo: COL0008924 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigaciones en derecho procesal', 'Código del Grupo: COL0043979 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigaciones en ingeniería civil (gici)', 'Código del Grupo: COL0025667 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigaciones en negocios y relaciones internacionales', 'Código del Grupo: COL0137359 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de investigaciones en proyectos gip3', 'Código del Grupo: COL0123416 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigaciones en sistema y control penal', 'Código del Grupo: COL0166619 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de investigaciones las américas', 'Código del Grupo: COL0137162 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: PROMOTORA MÉDICA LAS AMÉRICAS S.A.
Municipio: Medellín'),
(12,'Grupo de investigaciones regionales en democracia, desarrollo, conflicto y justicia', 'Código del Grupo: COL0008862 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias políticas
Institución aval: Instituto Popular De Capacitación - I.P.C. (Antes Corporación Promoción Popular)
Municipio: Medellín'),
(12,'Grupo de investigaciones sociojurídicas corporación universitaria uniremington (gisor)', 'Código del Grupo: COL0063139 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Grupo de investigaciones y consultorías contables - Gicco - Udea-', 'Código del Grupo: COL0085306 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de investigaciones y mediciones ambientales - gema', 'Código del Grupo: COL0008998 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de macroeconomía aplicada', 'Código del Grupo: COL0013539 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de magnetismo y simulación', 'Código del Grupo: COL0047009 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de manejo eficiente de la energía, gimel', 'Código del Grupo: COL0010477 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de materia condensada-udea', 'Código del Grupo: COL0033319 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de materiales poliméricos', 'Código del Grupo: COL0069779 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de materiales preciosos (mapre)', 'Código del Grupo: COL0064253 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de medicina comportamental del sueño', 'Código del Grupo: COL0052913 | Clasificación:  C
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Ciencias de la salud
Institución aval: Universidad Cooperativa De Colombia
Municipio: Envigado'),
(12,'Grupo de mineralogía aplicada y bioprocesos (gmab)', 'Código del Grupo: COL0006465 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Biotecnología Industrial
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de modelación y computación científica', 'Código del Grupo: COL0120808 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Otras Ciencias Naturales
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Grupo de neurociencias de Antioquia', 'Código del Grupo: COL0010744 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de nutrición y tecnología de alimentos', 'Código del Grupo: COL0010771 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de óptica aplicada', 'Código del Grupo: COL0011527 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Grupo de óptica y espectroscopía', 'Código del Grupo: COL0011536 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo de óptica y fotónica', 'Código del Grupo: COL0010789 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de parasitología, Universidad de Antioquia', 'Código del Grupo: COL0007506 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de promoción e investigación en mecánica aplicada gpima', 'Código del Grupo: COL0204229 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad Nacional de Colombia
Municipio: Medellín'),
(12,'Grupo de química-física teórica', 'Código del Grupo: COL0004399 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de reumatología Universidad de Antioquia - grua-', 'Código del Grupo: COL0010959 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo de sistemas energéticos', 'Código del Grupo: COL0011008 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo de soldadura', 'Código del Grupo: COL0028829 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingeniería mecánica
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Grupo de tomografía e inversión', 'Código del Grupo: COL0154799 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo de tribología y superficies', 'Código del Grupo: COL0011124 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo del cemento y materiales de construcción', 'Código del Grupo: COL0070269 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo didáctica de la educación superior', 'Código del Grupo: COL0006563 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo endocrinología y metabolismo- gem', 'Código del Grupo: COL0035547 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo estudios de derecho y política', 'Código del Grupo: COL0146484 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo estudios de opinión', 'Código del Grupo: COL0002099 | Clasificación:  B
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo estudios del territorio', 'Código del Grupo: COL0008334 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo herpetológico de Antioquia', 'Código del Grupo: COL0007373 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo historia de la salud', 'Código del Grupo: COL0007382 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y arqueología
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Grupo htm', 'Código del Grupo: COL0081549 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Geografía Social y Económica
Institución aval: GRUPO HTM
Municipio: Medellín'),
(12,'Grupo interáreas de álgebra, geometría y topología', 'Código del Grupo: COL0063292 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo interdisciplinario de estudios moleculares', 'Código del Grupo: COL0007462 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo interdisciplinario de estudios sociales - gies', 'Código del Grupo: COL0116073 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Grupo interdisciplinario para el desarrollo del pensamiento y la acción dialógica gidpad', 'Código del Grupo: COL0077387 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Grupo interinstitucional de investigación en geometría y topología', 'Código del Grupo: COL0028023 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: FUNDACIÓN UNIVERSIDAD DEL NORTE | Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo kimera', 'Código del Grupo: COL0150398 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo malaria', 'Código del Grupo: COL0007524 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo mapeo genético', 'Código del Grupo: COL0057491 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo medicina molecular y de translación', 'Código del Grupo: COL0140139 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo neuropsicología y conducta', 'Código del Grupo: COL0007551 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: UNIVERSIDAD DE SAN BUENAVENTURA - SEDE MEDELLÍN | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo para el estudio de las enfermedades cardiovasculares', 'Código del Grupo: COL0135865 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo pulpa y papel', 'Código del Grupo: COL0007579 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Grupo reproducción', 'Código del Grupo: COL0007631 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Grupo sumar', 'Código del Grupo: COL0092839 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Fundación Universitaria Maria Cano
Municipio: Medellín'),
(12,'Grupo tandem en nano-bio-física', 'Código del Grupo: COL0202529 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Biotecnología en Salud
Institución aval: UNIVERSIDAD DE ANTIOQUIA
Municipio: Medellín'),
(12,'Grupo teleinformática y teleautomatica', 'Código del Grupo: COL0086789 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Grupo vericel', 'Código del Grupo: COL0078089 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Hegemonía, guerras y conflictos', 'Código del Grupo: COL0079004 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Hematopatologia molecular', 'Código del Grupo: COL0043226 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Hidrología y modelación de ecosistemas', 'Código del Grupo: COL0068083 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Hipertrópico: convergencia entre arte y tecnología', 'Código del Grupo: COL0090369 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Historia cultural, memoria y patrimonio', 'Código del Grupo: COL0120405 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Historia de la práctica pedagógica en Colombia', 'Código del Grupo: COL0011886 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: UNIVERSIDAD DEL VALLE | PONTIFICIA UNIVERSIDAD JAVERIANA | Universidad Nacional de Colombia - Sede Medellín | UNIVERSIDAD PEDAGÓGICA NACIONAL | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Historia empresarial EAFIT', 'Código del Grupo: COL0011732 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Historia moderna de américa: dominación, resistencia y creación cultural', 'Código del Grupo: COL0037327 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y arqueología
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Historia moderna y contemporánea', 'Código del Grupo: COL0043066 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Historia, espacio y cultura', 'Código del Grupo: COL0153335 | Clasificación:  A1
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Historia, territorio y poblamiento en Colombia', 'Código del Grupo: COL0050641 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras historias
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Historia, trabajo, sociedad y cultura', 'Código del Grupo: COL0056699 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Holográfico - comunicación gráfica publicitaria', 'Código del Grupo: COL0121046 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Hombre, proyecto y ciudad', 'Código del Grupo: COL0049417 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Bello'),
(12,'Humanismo, sociedad y organizaciones', 'Código del Grupo: COL0015446 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad Pontificia Bolivariana - Sede Medellín
Municipio: Medellín'),
(12,'Humanitas', 'Código del Grupo: COL0105365 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'I&d argos', 'Código del Grupo: COL0148139 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: CEMENTOS ARGOS S.A
Municipio: Medellín'),
(12,'I^2 - innovación e ingeniería', 'Código del Grupo: COL0187529 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Icono', 'Código del Grupo: COL0099301 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Institución Universitaria Pascual Bravo
Municipio: Medellín'),
(12,'Icsa (investigación en ciencias sociales y administrativas)', 'Código del Grupo: COL0033149 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Politécnico Arzobispo Salazar Y Herrera
Municipio: Medellín'),
(12,'Identic', 'Código del Grupo: COL0163565 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Corporación Universitaria de Sabaneta
Municipio: Sabaneta'),
(12,'Identidades e imaginarios políticos', 'Código del Grupo: COL0001914 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y arqueología
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Identificación genética - identigen', 'Código del Grupo: COL0035429 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Idinnov', 'Código del Grupo: COL0173159 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: IDINNOV S.A.S
Municipio: Medellín'),
(12,'Ignea', 'Código del Grupo: COL0137269 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Imark - grupo de investigación en marketing', 'Código del Grupo: COL0096776 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Impacto de componentes alimentarios en la salud', 'Código del Grupo: COL0083811 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Inamod grupo de investigación aplicada en moda y diseño', 'Código del Grupo: COL0158921 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Itagüí'),
(12,'Inca-ces', 'Código del Grupo: COL0038665 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: INSTITUTO COLOMBIANO DE MEDICINA TROPICAL - Universidad CES | Universidad CES
Municipio: Medellín'),
(12,'Incas-innovación y gestión de cadenas de abastecimiento', 'Código del Grupo: COL0031851 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: I.P.S. Universitaria | Universidad de Antioquia | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Infección y cáncer', 'Código del Grupo: COL0012328 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Infettare', 'Código del Grupo: COL0112548 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Información y gestión', 'Código del Grupo: COL0050749 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Informed', 'Código del Grupo: COL0036212 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Otras Ciencias Médicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ingeco de Unaula', 'Código del Grupo: COL0081343 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Ingeniería agrícola', 'Código del Grupo: COL0073181 | Clasificación:  A1
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ingeniería de software', 'Código del Grupo: COL0057357 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Otras ingenierías y tecnologías
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Ingeniería y métodos cuantitativos para la administración -imca', 'Código del Grupo: COL0194079 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Fundación Universitaria CEIPA
Municipio: Sabaneta'),
(12,'Ingeniería y sociedad (i&s)', 'Código del Grupo: COL0031216 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ingeniería y tecnologías de las organizaciones y de la sociedad (itos)', 'Código del Grupo: COL0092169 | Clasificación:  B
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Computación y ciencias de la información
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Ingeniería, ciencias ambientales e innovación', 'Código del Grupo: COL0059271 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Fundación Universitaria Católica del Norte
Municipio: Santa Rosa de Osos'),
(12,'Ingeniería, energía, exergía y sostenibilidad (iexs)', 'Código del Grupo: COL0107815 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Ingennova', 'Código del Grupo: COL0188724 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Médica
Institución aval: INSTITUTO COLOMBIANO DE MEDICINA TROPICAL - Universidad CES | Universidad CES
Municipio: Medellín'),
(12,'Inmunodeficiencias primarias', 'Código del Grupo: COL0012426 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Inmunomodulación', 'Código del Grupo: COL0038389 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Inmunovirología', 'Código del Grupo: COL0012444 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Innovaciencia', 'Código del Grupo: COL0072863 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Innovación conconcreto', 'Código del Grupo: COL0204069 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: CONSTRUCTORA CONCONCRETO S.A
Municipio: Medellín'),
(12,'Innovación ti: educación-empresa-estado', 'Código del Grupo: COL0206054 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Corporación Metusia
Municipio: Medellín'),
(12,'Innovación y desarrollo regional', 'Código del Grupo: COL0186588 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: INSTITUTO ASYS
Municipio: Rionegro'),
(12,'Innovación y empresarismo (guie)', 'Código del Grupo: COL0052403 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Innovación y gestión tecnológica', 'Código del Grupo: COL0057811 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Innovación, desarrollo y avances en endocrinología', 'Código del Grupo: COL0159428 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina básica
Institución aval: EMDEC S.A.S.
Municipio: Medellín'),
(12,'Instituto de minerales cimex', 'Código del Grupo: COL0047753 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Instrumentación científica e industrial', 'Código del Grupo: COL0037917 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Instrumentación científica y microelectrónica', 'Código del Grupo: COL0012589 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias físicas
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Intal', 'Código del Grupo: COL0056242 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: FUNDACIÓN INTAL
Municipio: Itagüí'),
(12,'Integración de soluciones con tecnologías de información y comunicación -git-', 'Código del Grupo: COL0033328 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingenierías eléctrica, electrónica e informática
Institución aval: Instituto Tecnológico Metropolitano De Medellín - I.T.M.
Municipio: Medellín'),
(12,'Inteligencia artificial en educación', 'Código del Grupo: COL0059923 | Clasificación:  A
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Computación y ciencias de la información
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Intelligent information systems lab. (antes ingeniería y software)', 'Código del Grupo: COL0025934 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Interacciones biológicas', 'Código del Grupo: COL0157399 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Interacciones nutricionales, metabólicas y reproductivas en bovinos', 'Código del Grupo: COL0048849 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Interconexión global y finanzas gigf', 'Código del Grupo: COL0165292 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institucion Universitaria Escolme
Municipio: Medellín'),
(12,'Intertelco', 'Código del Grupo: COL0125976 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: INTER-TELCO S.A.S
Municipio: Medellín'),
(12,'Invemac', 'Código del Grupo: COL0206152 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Administrative Management Consultants S.A.S.
Municipio: Medellín'),
(12,'Investigación anestesia ces - iaces', 'Código del Grupo: COL0083698 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Investigación clínica aplicada', 'Código del Grupo: COL0171235 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Fundación Hospitalaria San Vicente De Paúl | Fundacion Hospital San Vicente de Paul - Rionegro
Municipio: Medellín'),
(12,'Investigación clínica hgm - ces', 'Código del Grupo: COL0084149 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES | Hospital General de Medellín Luz Castro de Gutiérrez ESE.
Municipio: Medellín'),
(12,'Investigación clínica hptu', 'Código del Grupo: COL0116289 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: HOSPITAL PABLO TOBÓN URIBE
Municipio: Medellín'),
(12,'Investigación e innovación ambiental. Giiam', 'Código del Grupo: COL0135669 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Institución Universitaria Pascual Bravo
Municipio: Medellín'),
(12,'Investigación en geología ambiental gea', 'Código del Grupo: COL0029539 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Investigación estratégica', 'Código del Grupo: COL0138482 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Epm Telecomunicaciones S.A. E.S.P. - Epm. Telco S.A. | Asociación para la promoción de la innovación social basada en TIC´S
Municipio: Medellín'),
(12,'Investigación indec-ces', 'Código del Grupo: COL0125303 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES | Fundación Instituto Neurológico De Colombia
Municipio: Medellín'),
(12,'Investigación jurídico social', 'Código del Grupo: COL0190123 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Investigación y desarrollo de productos farmacéuticos', 'Código del Grupo: COL0041909 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: CENTRO DE LA CIENCIA Y LA INVESTIGACIÓN FARMACEUTICA | Universidad CES
Municipio: Sabaneta'),
(12,'Investigación, desarrollo y aplicación de marcadores moleculares', 'Código del Grupo: COL0070591 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: GENES
Municipio: Medellín'),
(12,'Investigación-creación fuba', 'Código del Grupo: COL0125125 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Fundación Universitaria Bellas Artes
Municipio: Medellín'),
(12,'Investigaciones jurídicas', 'Código del Grupo: COL0012669 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Investigaciones pirometalúrgicas y de materiales-gipimme', 'Código del Grupo: COL0012659 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Investigaciones teológicas - unac', 'Código del Grupo: COL0164894 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Isaii - innovación y sostenibilidad aplicadas a infraestructuras en ingeniería', 'Código del Grupo: COL0096195 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Jurídicas y sociales', 'Código del Grupo: COL0044822 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Justicia y conflicto', 'Código del Grupo: COL0008281 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Kavilando', 'Código del Grupo: COL0099651 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: grupo de investigacion y editorial kavilando
Municipio: Medellín'),
(12,'Kenosis', 'Código del Grupo: COL0102953 | Clasificación:  D
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras humanidades
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'La practica de enfermería en el contexto social', 'Código del Grupo: COL0013029 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Ciencias de la salud
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Laboratorio de cad/cam/cae', 'Código del Grupo: COL0013067 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Laboratorio de investigación en polímeros', 'Código del Grupo: COL0167869 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Laboratorio universitario de estudios sociales', 'Código del Grupo: COL0043549 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Láser  y  espectroscopía óptica', 'Código del Grupo: COL0068593 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Leet laboratorio de estudios y experimentación técnica en arquitectura', 'Código del Grupo: COL0028669 | Clasificación:  D
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Pontificia Bolivariana - Sede Medellín
Municipio: Medellín'),
(12,'Legal advance world - law', 'Código del Grupo: COL0161471 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Corporación Universitaria Americana
Municipio: Medellín'),
(12,'Lengua y cultura', 'Código del Grupo: COL0054346 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Lenguajes computacionales', 'Código del Grupo: COL0035402 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Liderazgo y organizaciones - liderorg', 'Código del Grupo: COL0053447 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Limnología básica y experimental y biología y taxonomía marina', 'Código del Grupo: COL0013263 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Limnología y recursos hídricos', 'Código del Grupo: COL0013272 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Lógica y computación', 'Código del Grupo: COL0013352 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Logística industrial-organizacional "gico"', 'Código del Grupo: COL0055666 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Máquinas inteligentes y reconocimiento de patrones', 'Código del Grupo: COL0123729 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Mastozoologia, U. De Antioquia', 'Código del Grupo: COL0068824 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Matemática, educación y sociedad-mes', 'Código del Grupo: COL0106193 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Matemáticas y aplicaciones', 'Código del Grupo: COL0000523 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Materia orgánica sedimentaria y análisis de imagen', 'Código del Grupo: COL0023985 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Materiales avanzados y energía', 'Código del Grupo: COL0021185 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Materiales cerámicos y vítreos', 'Código del Grupo: COL0007856 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Materiales nanoestructurados y biomodelación', 'Código del Grupo: COL0136693 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Nanotecnología
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Materiales para el mobiliario - matermob', 'Código del Grupo: COL0113975 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Servicio Nacional De Aprendizaje Regional Antioquia
Municipio: Itagüí'),
(12,'Materiales y sistemas de construcción corona', 'Código del Grupo: COL0130887 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería de los Materiales
Institución aval: SUMICOL S.A.S. | Corlanc S.A.S
Municipio: Sabaneta'),
(12,'Mathema-formación e investigación en educación matemática', 'Código del Grupo: COL0053803 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Centro de Investigaciones Educativas y Pedagógicas de la Asociación Sindical de Maestros del Municipio de Medellín | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Mecánica aplicada', 'Código del Grupo: COL0053099 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Mecánica computacional y análisis de estructuras', 'Código del Grupo: COL0156079 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: UNIVERSIDAD NACIONAL DE COLOMBIA SEDE BOGOTA
Municipio: Medellín'),
(12,'Mecatrónica y diseño de máquinas', 'Código del Grupo: COL0013399 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Mecánica
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Medicina tropical', 'Código del Grupo: COL0013648 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: INSTITUTO COLOMBIANO DE MEDICINA TROPICAL - Universidad CES | Universidad CES
Municipio: Sabaneta'),
(12,'Mejoramiento y producción de especies andinas y tropicales', 'Código del Grupo: COL0039484 | Clasificación:  C
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Agricultura, Silvicultura y Pesca
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid | Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Memorias vivas', 'Código del Grupo: COL0185713 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Museo Casa de la Memoria
Municipio: Medellín'),
(12,'Memphis', 'Código del Grupo: COL0174469 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: SOFTWARE ESTRATEGICO S.A.S
Municipio: Sabaneta'),
(12,'Mesh-coindexa - modelación espacial en fenómenos sociales y humanos', 'Código del Grupo: COL0186523 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: COINDEXA S.A.S.
Municipio: Medellín'),
(12,'Metellium', 'Código del Grupo: COL0087098 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Santo Tomás de Aquino - Sede Bogotá - Usta | Universidad Santo Tomás - Medellín
Municipio: Medellín'),
(12,'Metodología de la enseñanza de la química', 'Código del Grupo: COL0055924 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias químicas
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Metusia', 'Código del Grupo: COL0176652 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Corporación Metusia
Municipio: Medellín'),
(12,'Micología médica', 'Código del Grupo: COL0042069 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Micología médica y experimental', 'Código del Grupo: COL0013709 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Santander | Universidad Pontificia Bolivariana | Corporación para Investigaciones Biológicas | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Microbiodiversidad y bioprospección', 'Código del Grupo: COL0057704 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Microbiología ambiental', 'Código del Grupo: COL0040189 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Microbiología del suelo', 'Código del Grupo: COL0027151 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Microbiología molecular', 'Código del Grupo: COL0013746 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Microeconomía aplicada', 'Código del Grupo: COL0013808 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Microeconomía aplicada y  teoría económica.', 'Código del Grupo: COL0100519 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Economía y negocios
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Modelación con ecuaciones diferenciales', 'Código del Grupo: COL0024365 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Modelado matemático', 'Código del Grupo: COL0016229 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Modelamiento para la gestión de operaciones (gimgo)', 'Código del Grupo: COL0084219 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Modelamiento y análisis energía ambiente economía', 'Código del Grupo: COL0049696 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad de los Andes | Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Modesis', 'Código del Grupo: COL0093451 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingeniería civil
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Movimiento y salud', 'Código del Grupo: COL0058059 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Mundo organizacional', 'Código del Grupo: COL0164698 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporación Universitaria Remington
Municipio: Medellín'),
(12,'Músicas regionales', 'Código del Grupo: COL0016935 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Nacer, salud sexual y reproductiva', 'Código del Grupo: COL0061636 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Narrativas modernas y crítica del presente', 'Código del Grupo: COL0029119 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Neurociencia y cognición', 'Código del Grupo: COL0066202 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Neurociencias básicas y aplicadas (nba)', 'Código del Grupo: COL0117867 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Nutral', 'Código del Grupo: COL0187665 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Observatorio de la salud pública', 'Código del Grupo: COL0014206 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Observatorio en ciudad y territorio educador en Antioquia y en el noroccidente colombiano', 'Código del Grupo: COL0170828 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Geografía social y económica
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Observatorio público', 'Código del Grupo: COL0140622 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Observatos', 'Código del Grupo: COL0151377 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Oceánicos - grupo de oceanografía e ingeniería costera de la universidad nacional', 'Código del Grupo: COL0069699 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ocio expresiones motrices y sociedad', 'Código del Grupo: COL0001816 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Ofalmo-ciencia', 'Código del Grupo: COL0182284 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Clínica Oftalmológica de Antioquia | Instituto Nacional de Investigación en Oftamológia LTDA.
Municipio: Medellín'),
(12,'Olistica', 'Código del Grupo: COL0129474 | Clasificación:  D
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Ciencias de la salud
Institución aval: Universidad Cooperativa De Colombia
Municipio: Medellín'),
(12,'One health and veterinary innovative research and development (ohvri)', 'Código del Grupo: COL0183245 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Agrícolas
Subárea de Conocimiento: Ciencias Veterinarias
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Óptica y procesamiento opto-digital', 'Código del Grupo: COL0014135 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Físicas
Institución aval: Universidad Nacional de Colombia
Municipio: Medellín'),
(12,'Optimización matemática de procesos: óptimo', 'Código del Grupo: COL0151419 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Matemática
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Orbis iuris', 'Código del Grupo: COL0164357 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Fundación Universitaria Autónoma de las Américas
Municipio: Medellín'),
(12,'Orygen - organización y gerencia', 'Código del Grupo: COL0073388 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Fundación Universitaria CEIPA
Municipio: Sabaneta'),
(12,'Palinología y paleoecología', 'Código del Grupo: COL0014538 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias de la tierra y medioambientales
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Pava-aprende', 'Código del Grupo: COL0115568 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Fundación Universitaria Católica del Norte
Municipio: Santa Rosa de Osos'),
(12,'Paz, educación y territorios', 'Código del Grupo: COL0015123 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Sociología
Institución aval: Corporación Región
Municipio: Medellín'),
(12,'Pedagogía y didáctica', 'Código del Grupo: COL0073913 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Fundación Universitaria Católica del Norte | Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Pedagogía y didáctica de las lenguas extranjeras', 'Código del Grupo: COL0048821 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Pedagogía y didácticas de los saberes', 'Código del Grupo: COL0019909 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Pedagogía, cultura y sociedad', 'Código del Grupo: COL0153863 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Corporación Universitaria Adventista
Municipio: Medellín'),
(12,'Pediatría ces', 'Código del Grupo: COL0029307 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Pensar ciudad. Tecnologías del hábitat', 'Código del Grupo: COL0089557 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Nacional de Colombia - Sede Medellín | Arkebios S.A.S.
Municipio: Medellín'),
(12,'Periodoncia, salud y educación', 'Código del Grupo: COL0082897 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Perspectivas de investigación en educación en ciencias', 'Código del Grupo: COL0150333 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Pes practicas corporales, sociedad, educación-currículo', 'Código del Grupo: COL0074679 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Planeamiento minero', 'Código del Grupo: COL0113509 | Clasificación:  Reconocido
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Pluriverso', 'Código del Grupo: COL0060693 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Poder y nuevas subjetividades. Otros lugares de lo político.', 'Código del Grupo: COL0151896 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Políticas sociales y servicios de salud', 'Código del Grupo: COL0014789 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Polygesta - política, legislación y gestión ambiental', 'Código del Grupo: COL0146269 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Posgrado en aprovechamiento de recursos hidráulicos', 'Código del Grupo: COL0014823 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Prácticas, saberes y representaciones en iberoamérica', 'Código del Grupo: COL0015221 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y arqueología
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Probióticos: prospección funcional y metabolitos', 'Código del Grupo: COL0182275 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Procesamiento digital de señales para sistemas en tiempo real', 'Código del Grupo: COL0193018 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Proceso penal y delito', 'Código del Grupo: COL0165129 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Procesos de formación en el contexto latinoamericano', 'Código del Grupo: COL0178109 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Procesos fisicoquímicos aplicados', 'Código del Grupo: COL0014897 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Procesos químicos industriales', 'Código del Grupo: COL0147427 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Procont: procesos, entornos y desarrollo contable', 'Código del Grupo: COL0083079 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Corporación Universitaria Americana
Municipio: Medellín'),
(12,'Producción, apropiación y circulación de saberes procircas', 'Código del Grupo: COL0015016 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Historia y Arqueología
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Producción, estructura y aplicación de biomoléculas', 'Código del Grupo: COL0015025 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Productos naturales marinos', 'Código del Grupo: COL0015043 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Programa de estudio y control de enfermedades tropicales', 'Código del Grupo: COL0015099 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Programa de ofidismo / escorpionismo Universidad de Antioquia', 'Código del Grupo: COL0014476 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Promoción de la salud', 'Código del Grupo: COL0015159 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: La Pintada'),
(12,'Promoción y prevención farmacéutica', 'Código del Grupo: COL0074661 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Psicoanálisis, sujeto y sociedad', 'Código del Grupo: COL0015249 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Psicolingüística y prosodia', 'Código del Grupo: COL0139129 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Idiomas y Literatura
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Psicología dinámica', 'Código del Grupo: COL0119469 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Psicología e intervenciones online', 'Código del Grupo: COL0072854 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Fundación Universitaria Católica del Norte
Municipio: Santa Rosa de Osos'),
(12,'Psicología y neurociencias', 'Código del Grupo: COL0137028 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Psicología y procesos clínico-sociales 2', 'Código del Grupo: COL0166155 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Psicología, psicoanálisis y conexiones (psyconex)', 'Código del Grupo: COL0015295 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Psicología, salud y sociedad', 'Código del Grupo: COL0035986 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Psique y sociedad', 'Código del Grupo: COL0079219 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Fundación Universitaria Maria Cano
Municipio: Medellín'),
(12,'Pure chemistry laboratorio de investigación y producción ecológica', 'Código del Grupo: COL0173866 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Otras ciencias naturales
Institución aval: Pure Chemistry S.A.S.
Municipio: Medellín'),
(12,'Pvg + arquitectos', 'Código del Grupo: COL0172063 | Clasificación:  C
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Otras Humanidades
Institución aval: PVG ARQUITECTOS S.A.S
Municipio: Medellín'),
(12,'Qualipro- grupo de investigación en calidad y productividad', 'Código del Grupo: COL0042559 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Institución Universitaria Pascual Bravo
Municipio: Medellín'),
(12,'Química básica, aplicada y ambiente. Alquimia', 'Código del Grupo: COL0095769 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Instituto Tecnológico Metropolitano de Medellín
Municipio: Medellín'),
(12,'Química de los productos naturales y los alimentos', 'Código del Grupo: COL0052289 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Química de plantas colombianas', 'Código del Grupo: COL0015329 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Química de recursos energéticos y medio ambiente', 'Código del Grupo: COL0015393 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Química orgánica de productos naturales', 'Código del Grupo: COL0015339 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Radiología ces', 'Código del Grupo: COL0169899 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Ratio juris', 'Código del Grupo: COL0041079 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Recursos estratégicos, región y dinámicas socioambientales', 'Código del Grupo: COL0015526 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Sociología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Recursos genéticos y mejoramiento de frutales andinos', 'Código del Grupo: COL0043048 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias agrícolas
Subárea de Conocimiento: Agricultura, silvicultura y pesca
Institución aval: Corporación Colombiana De Investigación Agropecuaria - Corpoica
Municipio: Rionegro'),
(12,'Red (research and enterprise development)', 'Código del Grupo: COL0047539 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Redaire - Unal', 'Código del Grupo: COL0015428 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Redes y actores sociales', 'Código del Grupo: COL0114177 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Sociología
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Rehabilitación de estructuras de madera, acero, hormigón y compuestas', 'Código del Grupo: COL0072667 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Ingeniería civil
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Rehabilitación en salud', 'Código del Grupo: COL0015599 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: I.P.S. Universitaria | Universidad de Antioquia | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Religión, cultura y sociedad', 'Código del Grupo: COL0015669 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Responsabilidad social y desarrollo sostenible', 'Código del Grupo: COL0096373 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Corporación Universitaria Minuto de Dios - Sede Bello
Municipio: Bello'),
(12,'Respuesta social en salud', 'Código del Grupo: COL0062043 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Restauración ecológica de tierras degradadas en el trópico', 'Código del Grupo: COL0136011 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Ricerca', 'Código del Grupo: COL0146063 | Clasificación:  D
Área de Conocimiento: Ciencias agrícolas
Subárea de Conocimiento: Ciencias veterinarias
Institución aval: Corporación Universitaria de Sabaneta
Municipio: Sabaneta'),
(12,'Rise', 'Código del Grupo: COL0079819 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Rituales y construcción de identidades', 'Código del Grupo: COL0015759 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Otras ciencias sociales
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Rizosfera', 'Código del Grupo: COL0023929 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Corporación Colombiana De Investigación Agropecuaria - AGROSAVIA
Municipio: Rionegro'),
(12,'Rynova', 'Código del Grupo: COL0172789 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: RYMEL INGENIERIA ELECTRICA S.A.S.
Municipio: Copacabana'),
(12,'Saber, poder y derecho', 'Código del Grupo: COL0089889 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Sabio - sustancias activas y biotecnología', 'Código del Grupo: COL0139139 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Salud bucal y bienestar', 'Código del Grupo: COL0057339 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'salud clínica y quirúrgica UPB', 'Código del Grupo: COL0189285 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad Pontificia Bolivariana | Clínica Universitaria Bolivariana 
Municipio: Medellín'),
(12,'Salud comportamental y organizacional', 'Código del Grupo: COL0015965 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Psicología
Institución aval: Universidad de San Buenaventura - Sede Medellín
Municipio: Medellín'),
(12,'Salud de las mujeres', 'Código del Grupo: COL0016031 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Salud mental UdeA', 'Código del Grupo: COL0015983 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Salud mental CES', 'Código del Grupo: COL0015992 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Salud sexual y cáncer', 'Código del Grupo: COL0001502 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Salud sexual y reproductiva clínica del prado-ces', 'Código del Grupo: COL0033462 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Salud y ambiente', 'Código del Grupo: COL0016049 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Salud y comunidad', 'Código del Grupo: COL0047449 | Clasificación:  A1
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Salud y sociedad', 'Código del Grupo: COL0027456 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Salud y sostenibilidad', 'Código del Grupo: COL0088881 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Seguridad y salud en el trabajo', 'Código del Grupo: COL0016004 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Senderos', 'Código del Grupo: COL0068655 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Institución Universitaria Tecnológico de Antioquia
Municipio: Medellín'),
(12,'Servicios ecosistemicos y cambio climático - secc', 'Código del Grupo: COL0136933 | Clasificación:  B
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias biológicas
Institución aval: Fundación Jardin Botánico Joaquín Antonio Uribe
Municipio: Medellín'),
(12,'Siafys (grupo de investigación en actividad física y salud )', 'Código del Grupo: COL0040859 | Clasificación:  C
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Ciencias de la salud
Institución aval: Politecnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Siete, sistema de investigación educativa, todos por la excelencia', 'Código del Grupo: COL0134849 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Escuela de Tecnologías de Antioquia
Municipio: Medellín'),
(12,'Sig y territorio', 'Código del Grupo: COL0092605 | Clasificación:  D
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias de la tierra y medioambientales
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Simulación de comportamientos de sistemas (sicosis)', 'Código del Grupo: COL0016139 | Clasificación:  B
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Computación y ciencias de la información
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Simulación, diseño, control y optimización de procesos - sidcop', 'Código del Grupo: COL0056574 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Síntesis y biosíntesis de metabolitos naturales', 'Código del Grupo: COL0069689 | Clasificación:  B
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Químicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín | Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Sistema de investigación tecnológica sit marco', 'Código del Grupo: COL0019445 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Institución Universitaria Marco Fidel Suárez
Municipio: Bello'),
(12,'Sistema penitenciario', 'Código del Grupo: COL0102809 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Derecho
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Sistemas complejos y química del silicio', 'Código del Grupo: COL0137538 | Clasificación:  C
Área de Conocimiento: Ciencias naturales
Subárea de Conocimiento: Ciencias químicas
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Sistemas de información en salud', 'Código del Grupo: COL0152688 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Sistemas de información y sociedad del conocimiento - sisco', 'Código del Grupo: COL0075195 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Sistemas embebidos e inteligencia computacional - sistemic', 'Código del Grupo: COL0010717 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingenierías Eléctrica, Electrónica e Informática
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Sistemas en operaciones y desarrollo aplicado', 'Código del Grupo: COL0084989 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Otras ingenierías y tecnologías
Institución aval: Universidad Cooperativa De Colombia
Municipio: Medellín'),
(12,'Sistemas inteligentes web (sintelweb)', 'Código del Grupo: COL0059834 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Sistemas simbióticos', 'Código del Grupo: COL0094789 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Smo comunicación, arte y convergencia cultural', 'Código del Grupo: COL0129099 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Politécnico Colombiano Jaime Isaza Cadavid
Municipio: Medellín'),
(12,'Sobiotech', 'Código del Grupo: COL0138203 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: SOLUCIONES BIOTECNOLOGICAS Y AGROAMBIENTALES S.A.S
Municipio: Guarne'),
(12,'Sociedad, política e historias conectadas', 'Código del Grupo: COL0044386 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias Políticas
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Socioantropología de la alimentación', 'Código del Grupo: COL0084935 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Solar, earth, and planetary physics group (seap)', 'Código del Grupo: COL0161631 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias de la Tierra y Medioambientales
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Somos palabra: formación y contextos', 'Código del Grupo: COL0123828 | Clasificación:  C
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Otras ciencias sociales
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Sostenibilidad, infraestructura y territorio - site', 'Código del Grupo: COL0006939 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Escuela de Ingeniería de Antioqua
Municipio: Envigado'),
(12,'Summa', 'Código del Grupo: COL0054631 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Taxonomía y ecología de hongos', 'Código del Grupo: COL0011769 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Tecnologías en salud', 'Código del Grupo: COL0066356 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Básica
Institución aval: Universidad CES
Municipio: Medellín'),
(12,'Tecnologías para la producción', 'Código del Grupo: COL0028453 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Teledeteccion y manejo forestal', 'Código del Grupo: COL0026074 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias agrícolas
Subárea de Conocimiento: Agricultura, silvicultura y pesca
Institución aval: Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Teología y pastoral', 'Código del Grupo: COL0154519 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Otras ciencias sociales
Institución aval: Fundación Universitaria Católica del Norte
Municipio: Medellín'),
(12,'Teoría, práctica e historia del arte en Colombia', 'Código del Grupo: COL0011053 | Clasificación:  A
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Terapia celular, hospital universitario San Vicente de Paúl', 'Código del Grupo: COL0029002 | Clasificación:  C
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina clínica
Institución aval: Hospital Universitario San Vicente Fundación
Municipio: Medellín'),
(12,'Termodinámica aplicada y energías alternativas', 'Código del Grupo: COL0071024 | Clasificación:  A1
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Química
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Termomec', 'Código del Grupo: COL0054239 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Otras Ingenierías y Tecnologías
Institución aval: Universidad Cooperativa de Colombia
Municipio: Medellín'),
(12,'Territorio', 'Código del Grupo: COL0016559 | Clasificación:  A1
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Geografía Social y Económica
Institución aval: Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Territorio pedagógico', 'Código del Grupo: COL0065644 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Ciencias de la educación
Institución aval: Universidad Autónoma Latinoamericana - Unaula
Municipio: Medellín'),
(12,'Tetrix marketing - grupo de investigación en marketing', 'Código del Grupo: COL0139603 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Economía y Negocios
Institución aval: Universidad de Medellin
Municipio: Medellín'),
(12,'Trabajo, riesgo y desigualdades', 'Código del Grupo: COL0072943 | Clasificación:  D
Área de Conocimiento: Ciencias sociales
Subárea de Conocimiento: Sociología
Institución aval: Corporación Gaea | Universidad Nacional de Colombia - Oficial
Municipio: Medellín'),
(12,'Transepto. Arquitectura sin fronteras', 'Código del Grupo: COL0184547 | Clasificación:  Reconocido
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Trauma y cirugía', 'Código del Grupo: COL0016612 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Unidad de bacteriología y micobacterias, cib-upb', 'Código del Grupo: COL0016748 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Otras Ciencias Médicas
Institución aval: Universidad Pontificia Bolivariana | Corporación para Investigaciones Biológicas
Municipio: Medellín'),
(12,'Unidad de biotecnología vegetal', 'Código del Grupo: COL0016757 | Clasificación:  A
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Unidad de fitosanidad y control biológico', 'Código del Grupo: COL0016775 | Clasificación:  C
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Ciencias Biológicas
Institución aval: Universidad de Santander | Corporación para Investigaciones Biológicas | INSTITUCIÓN UNIVERSITARIA COLEGIO MAYOR DE ANTIOQUIA
Municipio: Medellín'),
(12,'Unidad de gestión del conocimiento', 'Código del Grupo: COL0137251 | Clasificación:  C
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Otras Ciencias Sociales
Institución aval: Universidad Católica de Oriente
Municipio: Rionegro'),
(12,'Unidad de inmunología clínica y reumatología - unir - clínica universitaria bolivariana', 'Código del Grupo: COL0057348 | Clasificación:  B
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Medicina Clínica
Institución aval: Universidad Pontificia Bolivariana | Clínica Universitaria Bolivariana - Universidad Pontificia Bolivariana
Municipio: Medellín'),
(12,'Unidad de investigación clínica', 'Código del Grupo: COL0185329 | Clasificación:  C
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: Corporación para Investigaciones Biológicas
Municipio: Medellín'),
(12,'Unidad de investigación e innovación humax pharmaceutical', 'Código del Grupo: COL0128744 | Clasificación:  Reconocido
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Ciencias de la salud
Institución aval: Humax Pharmaceutical
Municipio: La Estrella'),
(12,'Unipluriversidad', 'Código del Grupo: COL0001398 | Clasificación:  B
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Ciencias de la Educación
Institución aval: Universidad de Antioquia - Sede Carmen de Viboral
Municipio: Medellín'),
(12,'Universus - grupo de investigación en ciencia y humanidades', 'Código del Grupo: COL0123238 | Clasificación:  A1
Área de Conocimiento: Ciencias Naturales
Subárea de Conocimiento: Computación y Ciencias de la Información
Institución aval: Instituto Antioqueño de Investigación
Municipio: Medellín'),
(12,'Unun vertere', 'Código del Grupo: COL0016695 | Clasificación:  C
Área de Conocimiento: Ciencias médicas y de la salud
Subárea de Conocimiento: Medicina clínica
Institución aval: Universidad de Antioquia
Municipio: Medellín'),
(12,'Urbam - centro de estudios urbanos y ambientales', 'Código del Grupo: COL0137298 | Clasificación:  B
Área de Conocimiento: Humanidades
Subárea de Conocimiento: Arte
Institución aval: Universidad EAFIT
Municipio: Medellín'),
(12,'Urbanitas', 'Código del Grupo: COL0044779 | Clasificación:  A
Área de Conocimiento: Ciencias Sociales
Subárea de Conocimiento: Periodismo y Comunicaciones
Institución aval: Universidad Católica Luis Amigó
Municipio: Medellín'),
(12,'Vedas', 'Código del Grupo: COL0182195 | Clasificación:  C
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Nanotecnología
Institución aval: VEDAS CORPORACION DE INVESTIGACION E INNOVACION
Municipio: Medellín'),
(12,'Vias y transporte (vitra)', 'Código del Grupo: COL0016917 | Clasificación:  A
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Civil
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín'),
(12,'Vidarium', 'Código del Grupo: COL0100411 | Clasificación:  A
Área de Conocimiento: Ciencias Médicas y de la Salud
Subárea de Conocimiento: Ciencias de la Salud
Institución aval: CORPORACIÓN VIDARIUM, CENTRO DE INVESTIGACIÓN EN NUTRICIÓN SALUD Y BIENESTAR
Municipio: Medellín'),
(12,'Woma investigación', 'Código del Grupo: COL0160429 | Clasificación:  C
Área de Conocimiento: Ingeniería y tecnología
Subárea de Conocimiento: Otras ingenierías y tecnologías
Institución aval: WOMA S.A.S
Municipio: Medellín'),
(12,'Yacimientos de hidrocarburos', 'Código del Grupo: COL0017019 | Clasificación:  B
Área de Conocimiento: Ingeniería y Tecnología
Subárea de Conocimiento: Ingeniería Ambiental
Institución aval: Universidad Nacional de Colombia - Sede Medellín
Municipio: Medellín');