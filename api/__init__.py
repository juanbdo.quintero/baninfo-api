from flask import Flask, jsonify, request
from flask_cors import CORS
from api.shared.utils import response
from api.shared.constants import HTTP_ERRORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager

def create_api(env='dev'):
    # Inicializar Aplicacion de Flask
    app = Flask(__name__)
    CORS(app)

    with app.app_context():
        # Configuracion base de app
        config(app, env)

        # Inicializar la BD de Baninfo
        initialize_db(app)

        # Manejador/Captura de errores
        error_handlers(app)

        # Registrar rutas con vistas del api
        routes(app)

    return app

def config(api, env):
    # Definir variables de configuracion (desde config.py)
    environments = {
        'dev': 'config.DevConfig',
        'test': 'config.TestConfig',
        'prod': 'config.ProdConfig'
    }
    env_config = environments.get(env, 'config.Config')
    api.config.from_object(env_config)

    JWTManager(api)

def routes(api):
    root_path = api.config.get('ROOT_PATH', '/')

    # pylint: disable=unused-variable
    @api.route(root_path)
    def index():
        return jsonify({'api': 'BanInfo API'}), 200

    # Register routes
    from api.routes import (
        auth,
        tipo_documento,
        eje,
        condicion,
        usuario,
        organizacion,
        certificacion,
        documento
    )
    api.register_blueprint(auth, url_prefix=f'{root_path}auth')
    api.register_blueprint(tipo_documento, url_prefix=f'{root_path}tipos-documentos')
    api.register_blueprint(eje, url_prefix=f'{root_path}ejes')
    api.register_blueprint(condicion, url_prefix=f'{root_path}condiciones')
    api.register_blueprint(usuario, url_prefix=f'{root_path}usuarios')
    api.register_blueprint(organizacion, url_prefix=f'{root_path}organizaciones')
    api.register_blueprint(certificacion, url_prefix=f'{root_path}certificaciones')
    api.register_blueprint(documento, url_prefix=f'{root_path}documentos')

def error_handlers(app):
    error_logger = app.config.get('ERROR_LOGGER', None)

    # pylint: disable=unused-variable,unused-argument
    @app.errorhandler(404)
    def not_found(error):
        return response(**HTTP_ERRORS['NotFound'])

    @app.errorhandler(405)
    def method_not_allow(error):
        return response(**HTTP_ERRORS['MethodNotAllowed'])

    @app.errorhandler(500)
    def internal_server_error(error):
        error_logger.error(error)
        return response(**HTTP_ERRORS['InternalServerError'], error=str(error))

    @app.errorhandler(Exception)
    def exception(error):
        error_logger.error(f'EXCEPTION: {error}')
        return response(**HTTP_ERRORS['InternalServerError'], error=str(error))

def initialize_db(app):
    db = SQLAlchemy()
    db.init_app(app)
    app.config['DB'] = db

    ma = Marshmallow(app)
    app.config['MA_SERIALIZER'] = ma
