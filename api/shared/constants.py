HTTP_ERRORS = {
    'BadRequest': {
        'status_code': 400,
        'message':'Bad Request'
    },
    'Unauthorized': {
        'status_code': 401,
        'message': 'Invalid credentials'
    },
    'Forbidden':{
        'status_code': 403,
        'message':'Forbidden'
    },
    'NotFound': {
        'status_code': 404,
        'message':'Endpoint Not Found'
    },
    'MethodNotAllowed': {
        'status_code': 405,
        'message':'Method Not Allowed'
    },
    'RequestTimeout': {
        'status_code': 408,
        'message':'Request Timeout'
    },
    'InternalServerError': {
        'status_code': 500,
        'message':'Internal Server Error'
    },
    'SchemaValidationError': {
        'status_code': 400,
        'message': 'Request is missing required fields'
    }
}
