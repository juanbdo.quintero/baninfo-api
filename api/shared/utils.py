def response(*args, **kwargs):
    if args:
        kwargs['status_code'] = args[0]
    elif 'status_code' not in kwargs:
        kwargs['status_code'] = 200
    return  kwargs, kwargs['status_code']
