import json
from flask import Blueprint, request
from api.shared.utils import response
from api.db.models import Documento, insert, delete, update, select_by_label
from flask_jwt_extended import jwt_required
from api.db.serializers import (
    documento_schema as schema,
    documentos_schema as schema_many
)

documento = Blueprint('documento', __name__)

@documento.route('/')
@documento.route('/<int:id_>')
def get(id_=None):
    if id_ is None:
        items = Documento.query.all()
        return response(200, data=schema_many.dump(items))
    item = Documento.query.filter_by(codigo=id_).first()
    if item:
        return response(200, data=schema.dump(item))
    return response(404, message='Item no encontrado')

@documento.route('/eje/<int:eje_>')
def get_by_eje(eje_=None):
    if eje_:
        items = Documento.query.filter_by(eje=eje_)
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')

@documento.route('/tipo/<int:tipo_>')
def get_by_tipo(tipo_=None):
    if tipo_:
        items = Documento.query.filter_by(tipo=tipo_)
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')

@documento.route('/categoria/<categoria_>')
def get_by_categoria(categoria_=None):
    if categoria_:
        items = Documento.query.filter_by(categoria=categoria_)
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')

@documento.route('/etiqueta/<etiqueta_>')
def get_by_etiqueta(etiqueta_=None):
    if etiqueta_:
        items = select_by_label(etiqueta_)
        items_serialized = schema_many.dump(items)
        for item in items_serialized:
            etiquetas_list = json.loads(item.get('etiquetas'))
            item.update({'etiquetas': etiquetas_list})
        return response(200, data=items_serialized)
    return response(404, message='Items no encontrados')

@documento.route('/etiquetas')
def get_etiquetas():
    items = Documento.query.with_entities(Documento.etiquetas).all()
    etiquetas_list = []
    for item in schema_many.dump(items):
        etiquetas_list.extend(item.get('etiquetas'))
    etiquetas = list(set(etiquetas_list))
    if etiquetas:
        return response(200, data=etiquetas)
    return response(404, message='Items no encontrados')

@documento.route('/', methods=['POST'])
@documento.route('', methods=['POST'])
@jwt_required()
def post():
    body = request.json
    errors = schema.validate(body)
    if errors:
        return response(400, message='Body invalido', error=errors)
    item = Documento(**schema.load(body))
    insert(item)
    return response(201, message='Item creado')

@documento.route('/<int:id_>', methods=['PUT'])
@jwt_required()
def put(id_=None):
    body = request.json
    item = Documento.query.filter_by(codigo=id_)
    if item.first():
        update(item, body)
        return response(202, message='Item actualizado')
    return response(404, message='Actualización no realizada, item no encontrado')

@documento.route('/<int:id_>', methods=['DELETE'])
@jwt_required()
def delete_one(id_=None):
    item = Documento.query.filter_by(codigo=id_).first()
    if item:
        delete(item)
        return response(202, message='Item borrado')
    return response(404, message='Borrado no realizado, item no encontrado')
