from flask import Blueprint, request
from api.shared.utils import response
from api.db.models import Usuario, insert, delete, update
from flask_jwt_extended import jwt_required
from api.db.serializers import (
    usuario_schema as schema,
    usuarios_schema as schema_many,
    login_schema
)

usuario = Blueprint('usuario', __name__)

@usuario.route('/')
@usuario.route('/<string:identificacion_>')
@jwt_required()
def get(identificacion_=None):
    if identificacion_ is None:
        items = Usuario.query.all()
        return response(200, data=schema_many.dump(items))
    item = Usuario.query.filter_by(identificacion=identificacion_).first()
    if item:
        return response(200, data=schema.dump(item))
    return response(404, message='Item no encontrado')

@usuario.route('/', methods=['POST'])
@usuario.route('', methods=['POST'])
@jwt_required()
def post():
    body = request.json
    errors = schema.validate(body)
    if errors:
        return response(400, message='Body invalido', error=errors)
    body_login = {'correo': body.get('correo'), 'clave': body.get('clave')}
    error_clave = login_schema.validate(body_login)
    if error_clave:
        return response(400, message='Formato de clave invalido', error=error_clave)
    item = Usuario(**schema.load(body))
    insert(item)
    return response(201, message='Item creado')

@usuario.route('/<string:identificacion_>', methods=['PUT'])
@jwt_required()
def put(identificacion_=None):
    body = request.json
    item = Usuario.query.filter_by(identificacion=identificacion_)
    if item.first():
        update(item, body)
        return response(202, message='Item actualizado')
    return response(404, message='Actualización no realizada, item no encontrado')

@usuario.route('/<string:identificacion_>', methods=['DELETE'])
@jwt_required()
def delete_one(identificacion_=None):
    item = Usuario.query.filter_by(identificacion=identificacion_).first()
    if item:
        delete(item)
        return response(202, message='Item borrado')
    return response(404, message='Borrado no realizado, item no encontrado')

def user_exists(correo_=None, clave_=None):
    try:
        item = Usuario.query.filter_by(correo=correo_, clave=clave_)
        if item.first():
            return True, item.first()
        return False, 'Usuario/Clave invalido.'
    except Exception as e:
        return False, e
