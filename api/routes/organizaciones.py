from flask import Blueprint, request
from api.shared.utils import response
from api.db.models import Organizacion, Condicion, insert, delete, update, db
from flask_jwt_extended import jwt_required
from api.db.serializers import (
    organizacion_schema as schema,
    organizaciones_schema as schema_many
)

organizacion = Blueprint('organizacion', __name__)

@organizacion.route('/')
@organizacion.route('/<nombre_>')
def get(nombre_=None):
    if nombre_ is None:
        items = Organizacion.query.all()
        return response(200, data=schema_many.dump(items))
    item = Organizacion.query.filter_by(nombre=nombre_).first()
    if item:
        return response(200, data=schema.dump(item))
    return response(404, message='Item no encontrado')

@organizacion.route('/condicion/<int:condicion_>')
def get_by_condicion(condicion_=None):
    if condicion_:
        items = Organizacion.query.filter_by(condicion=condicion_)
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')

@organizacion.route('/eje/<int:eje_>')
def get_by_eje(eje_=None):
    if eje_:
        items = db.session.query(Organizacion).join(Condicion).filter(Condicion.eje == eje_).all()
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')


@organizacion.route('/', methods=['POST'])
@organizacion.route('', methods=['POST'])
@jwt_required()
def post():
    body = request.json
    errors = schema.validate(body)
    if errors:
        return response(400, message='Body invalido', error=errors)
    item = Organizacion(**schema.load(body))
    insert(item)
    return response(201, message='Item creado')

@organizacion.route('/<nombre_>', methods=['PUT'])
@jwt_required()
def put(nombre_=None):
    body = request.json
    item = Organizacion.query.filter_by(nombre=nombre_)
    if item.first():
        update(item, body)
        return response(202, message='Item actualizado')
    return response(404, message='Actualización no realizada, item no encontrado')

@organizacion.route('/<nombre_>', methods=['DELETE'])
@jwt_required()
def delete_one(nombre_=None):
    item = Organizacion.query.filter_by(nombre=nombre_).first()
    if item:
        delete(item)
        return response(202, message='Item borrado')
    return response(404, message='Borrado no realizado, item no encontrado')
