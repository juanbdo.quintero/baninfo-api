from flask import Blueprint, request
from api.shared.utils import response
from flask_jwt_extended import create_access_token
import datetime as dt

from api.db.serializers import login_schema, usuario_schema
from api.routes.usuarios import user_exists, post

auth = Blueprint('Auth', __name__)

@auth.route('/login/', methods=['POST'])
@auth.route('/login', methods=['POST'])
def login():
    body = request.json
    errors = login_schema.validate(body)
    if errors:
        return response(400, message='Body invalido para login', error=errors)

    correo = request.json.get('correo')
    clave = request.json.get('clave')

    user = user_exists(correo, clave)

    if user[0] is False:
        return response(401, message=str(user[1]))
    item = usuario_schema.dump(user[1])
    access_token = create_access_token(identity=item, expires_delta=dt.timedelta(minutes=90))
    return response(200, access_token=access_token)

@auth.route('/singup', methods=['POST'])
def singup():
    return post
