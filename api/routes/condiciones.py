from flask import Blueprint, request
from api.shared.utils import response
from api.db.models import Condicion, insert, delete, update
from flask_jwt_extended import jwt_required
from api.db.serializers import (
    condicion_schema as schema,
    condiciones_schema as schema_many
)

condicion = Blueprint('condicion', __name__)

@condicion.route('/')
@condicion.route('/<int:id_>')
def get(id_=None):
    if id_ is None:
        items = Condicion.query.all()
        return response(200, data=schema_many.dump(items))
    item = Condicion.query.filter_by(codigo=id_).first()
    if item:
        return response(200, data=schema.dump(item))
    return response(404, message='Item no encontrado')

@condicion.route('/eje/<int:eje_>')
def get_by_eje(eje_=None):
    if eje_:
        items = Condicion.query.filter_by(eje=eje_)
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')

@condicion.route('/', methods=['POST'])
@condicion.route('', methods=['POST'])
@jwt_required()
def post():
    body = request.json
    errors = schema.validate(body)
    if errors:
        return response(400, message='Body invalido', error=errors)
    item = Condicion(**schema.load(body))
    insert(item)
    return response(201, message='Item creado')

@condicion.route('/<int:id_>', methods=['PUT'])
@jwt_required()
def put(id_=None):
    body = request.json
    item = Condicion.query.filter_by(codigo=id_)
    if item.first():
        update(item, body)
        return response(202, message='Item actualizado')
    return response(404, message='Actualización no realizada, item no encontrado')

@condicion.route('/<int:id_>', methods=['DELETE'])
@jwt_required()
def delete_one(id_=None):
    item = Condicion.query.filter_by(codigo=id_).first()
    if item:
        delete(item)
        return response(202, message='Item borrado')
    return response(404, message='Borrado no realizado, item no encontrado')
