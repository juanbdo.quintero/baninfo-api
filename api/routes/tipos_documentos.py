from flask import Blueprint, request
from api.shared.utils import response
from api.db.models import TipoDocumento, insert, delete, update
from flask_jwt_extended import jwt_required
from api.db.serializers import (
    tipo_documento_schema as schema,
    tipos_documentos_schema as schema_many
)

tipo_documento = Blueprint('tipo-documento', __name__)

@tipo_documento.route('/')
@tipo_documento.route('/<int:id_>')
def get(id_=None):
    if id_ is None:
        items = TipoDocumento.query.all()
        return response(200, data=schema_many.dump(items))
    item = TipoDocumento.query.filter_by(codigo=id_).first()
    if item:
        return response(200, data=schema.dump(item))
    return response(404, message='Item no encontrado')

@tipo_documento.route('/', methods=['POST'])
@tipo_documento.route('', methods=['POST'])
@jwt_required()
def post():
    body = request.json
    errors = schema.validate(body)
    if errors:
        return response(400, message='Body invalido', error=errors)
    item = TipoDocumento(**schema.load(body))
    insert(item)
    return response(201, message='Item creado')

@tipo_documento.route('/<int:id_>', methods=['PUT'])
@jwt_required()
def put(id_=None):
    body = request.json
    item = TipoDocumento.query.filter_by(codigo=id_)
    if item.first():
        update(item, body)
        return response(202, message='Item actualizado')
    return response(404, message='Actualización no realizada, item no encontrado')

@tipo_documento.route('/<int:id_>', methods=['DELETE'])
@jwt_required()
def delete_one(id_=None):
    item = TipoDocumento.query.filter_by(codigo=id_).first()
    if item:
        delete(item)
        return response(202, message='Item borrado')
    return response(404, message='Borrado no realizado, item no encontrado')
