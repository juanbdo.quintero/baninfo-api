from flask import Blueprint, request
from api.shared.utils import response
from api.db.models import Certificacion, Condicion, insert, delete, update, db
from flask_jwt_extended import jwt_required
from api.db.serializers import (
    certificacion_schema as schema,
    certificaciones_schema as schema_many
)

certificacion = Blueprint('certificacion', __name__)

@certificacion.route('/')
@certificacion.route('/<int:id_>')
def get(id_=None):
    if id_ is None:
        items = Certificacion.query.all()
        return response(200, data=schema_many.dump(items))
    item = Certificacion.query.filter_by(codigo=id_).first()
    if item:
        return response(200, data=schema.dump(item))
    return response(404, message='Item no encontrado')

@certificacion.route('/condicion/<int:condicion_>')
def get_by_condicion(condicion_=None):
    if condicion_:
        items = Certificacion.query.filter_by(condicion=condicion_)
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')

@certificacion.route('/eje/<int:eje_>')
def get_by_eje(eje_=None):
    if eje_:
        items = db.session.query(Certificacion).join(Condicion).filter(Condicion.eje == eje_).all()
        return response(200, data=schema_many.dump(items))
    return response(404, message='Items no encontrados')

@certificacion.route('/', methods=['POST'])
@certificacion.route('', methods=['POST'])
@jwt_required()
def post():
    body = request.json
    errors = schema.validate(body)
    if errors:
        return response(400, message='Body invalido', error=errors)
    item = Certificacion(**schema.load(body))
    insert(item)
    return response(201, message='Item creado')

@certificacion.route('/<int:id_>', methods=['PUT'])
@jwt_required()
def put(id_=None):
    body = request.json
    item = Certificacion.query.filter_by(codigo=id_)
    if item.first():
        update(item, body)
        return response(202, message='Item actualizado')
    return response(404, message='Actualización no realizada, item no encontrado')

@certificacion.route('/<int:id_>', methods=['DELETE'])
@jwt_required()
def delete_one(id_=None):
    item = Certificacion.query.filter_by(codigo=id_).first()
    if item:
        delete(item)
        return response(202, message='Item borrado')
    return response(404, message='Borrado no realizado, item no encontrado')
