# pylint: disable=redefined-outer-name,invalid-name,unused-argument,unused-variable
# pylint: disable=R0201,E1101
from flask_unittest import ClientTestCase
from api import create_api
from tests.mocks import mock_data_fields, mock_message_fields

class TestAPIClient(ClientTestCase):

    app = create_api('test')

    @property
    def root_path(self):
        return self.app.config.get('ROOT_PATH', '')

    def setUp(self, client):
        pass

    def tearDown(self, client):
        pass

    def assert_fields(self, res, error=False, message=False):
        self.assertEqual(res.is_json, True)
        self.assertEqual(res.json['error'], error)
        fields = mock_message_fields if message or error else mock_data_fields
        self.assertEqual(sorted(res.json.keys()), fields)

class TestControllerClient(TestAPIClient):
    '''
    TestAPIClientController:\n
    This class test the response with an http client of a specific controller.
    '''

    def test_get_all(self, client):
        res = client.get(f'{self.root_path}/controller/')
        self.assertStatus(res, 200)
        self.assert_fields(res)
        res = client.get(f'{self.root_path}/controller')
        self.assertStatus(res, 200)
        self.assert_fields(res)

    def test_get_one(self, client):
        res = client.get(f'{self.root_path}/controller/1')
        self.assertStatus(res, 200)
        self.assert_fields(res)

    def test_get_one_not_found(self, client):
        res = client.get(f'{self.root_path}/controller/5')
        self.assertStatus(res, 404)
        self.assert_fields(res, error=True)

    def test_post(self, client):
        res = client.post(f'{self.root_path}/controller')
        self.assertStatus(res, 201)
        self.assert_fields(res, message=True)
        self.assertEqual(res.json.get('message'), 'created')

    def test_put(self, client):
        res = client.put(f'{self.root_path}/controller/2')
        self.assertStatus(res, 202)
        self.assert_fields(res, message=True)
        self.assertEqual(res.json.get('message'), 'updated')

    def test_put_not_found(self, client):
        res = client.put(f'{self.root_path}/controller/78')
        self.assertStatus(res, 404)
        self.assert_fields(res, error=True, message=True)
        self.assertIn('Not updated', res.json.get('message'))
        self.assertIn('not found', res.json.get('message'))

    def test_patch(self, client):
        res = client.patch(f'{self.root_path}/controller/1')
        self.assertStatus(res, 202)
        self.assert_fields(res, message=True)
        self.assertEqual(res.json.get('message'), 'patched')
