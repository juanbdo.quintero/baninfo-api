*Good Practices*
https://auth0.com/blog/best-practices-for-flask-api-development/
-Design your API endpoints with proper names and HTTP verbs
-How to properly structure your application
-Build your documentation from the code
-Testing
https:// python.plainenglish.io/flask-restful-apis-72e05f8d41fa

*Testing*
https://pypi.org/project/flask-unittest/

*Git Standars*
Conventional Commits
https://www.conventionalcommits.org/en/v1.0.0/
https://pypi.org/project/enforce-git-message/
https://enforce-git-message.readthedocs.io/en/latest/

chmod ug+x .git/hooks/*

Pre commit hooks
https://pre-commit.com/
https://pypi.org/project/pre-commit/

pre-commit run --all-files

*Installation*
pip3 install --user -r requirements-dev.txt
