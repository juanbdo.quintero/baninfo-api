# -*- coding: utf-8 -*-
from api import create_api

api = create_api()

# flask cli context setup
@api.shell_context_processor
def get_context():
    '''Objects exposed here will be automatically available from the shell.'''
    return dict(app=api)

# Run server
if __name__ == '__main__':
    api.run(host='0.0.0.0')
